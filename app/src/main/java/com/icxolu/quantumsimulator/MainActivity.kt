package com.icxolu.quantumsimulator

import android.content.Intent
import android.content.res.ColorStateList
import android.content.res.Configuration
import android.os.Bundle
import android.view.DragEvent
import android.view.Menu
import android.view.MenuItem

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

import ch.obermuhlner.math.big.BigDecimalMath

import com.icxolu.quantumsimulator.circuit.CircuitAdapter
import com.icxolu.quantumsimulator.databinding.ActivityMainBinding
import com.icxolu.quantumsimulator.gateList.GateListAdapter
import com.icxolu.quantumsimulator.gateList.dialog.CreateGateDialog
import com.icxolu.quantumsimulator.items.AddView
import com.icxolu.quantumsimulator.items.IItem
import com.icxolu.quantumsimulator.items.StateItem
import com.icxolu.quantumsimulator.items.UserGate
import com.icxolu.quantumsimulator.items.gates.*
import com.icxolu.quantumsimulator.items.itemDelegates.DragObject
import com.icxolu.quantumsimulator.utils.*

class MainActivity: AppCompatActivity(), CreateGateDialog.CreateGateDialogListener {
    companion object {
        const val CIRCUIT_ADAPTER = "circuit_adapter"
        private const val GATE_LIST_ADAPTER = "gate_list_adapter"
        const val CREATE_USER_GATE = 0
    }
    internal lateinit var binding: ActivityMainBinding
    private lateinit var gateListAdapter: GateListAdapter
    private lateinit var circuitAdapter: CircuitAdapter

    private val manyQubits: Boolean
        get() = circuitAdapter.lines > 5

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        initCircuitView(savedInstanceState)
        initGatesView(savedInstanceState)
        initCircuitButtons()
        initDeleteButton()

        initSimulate()
    }

    private fun initCircuitView(savedInstanceState: Bundle?) {
        circuitAdapter = if (savedInstanceState != null) {
            savedInstanceState.getParcelableSDK(CIRCUIT_ADAPTER)!!
        } else {
            val lst: MutableList<IItem> = MutableList(20) { IdentityGate() }
            lst.addAll(0, listOf(StateItem(), StateItem()))
            CircuitAdapter(lst)
        }
        circuitAdapter.activity = this

        binding.circuitView.apply {
            adapter = circuitAdapter
            layoutManager = GridLayoutManager(this@MainActivity, circuitAdapter.lines, GridLayoutManager.HORIZONTAL, false)
            addItemDecoration(ItemDecorationControlled())
        }
    }

    private fun initGatesView(savedInstanceState: Bundle?) {
        gateListAdapter = if (savedInstanceState != null) {
            savedInstanceState.getParcelableSDK(GATE_LIST_ADAPTER)!!
        } else {
            GateListAdapter(listOf(
                AddView(), HadamardGate(), NotGate(), YGate(), ZGate(), SGate(), SDaggerGate(),
                TGate(), TDaggerGate(), ControlledGate()
            ))
        }
        gateListAdapter.activity = this

        val spanCount = if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) 5 else 2
        binding.gatesList.apply {
            adapter = gateListAdapter
            layoutManager = GridLayoutManager(this@MainActivity, spanCount, RecyclerView.VERTICAL, false)
        }
    }

    private fun initCircuitButtons() {

        binding.menuItemAddQubit.setOnClickListener {
            circuitAdapter.addLine(IdentityGate())
            (binding.circuitView.layoutManager as GridLayoutManager).spanCount = circuitAdapter.lines
        }
        binding.menuItemRemoveQubit.setOnClickListener {
            if (circuitAdapter.removeLine()) {
                (binding.circuitView.layoutManager as GridLayoutManager).spanCount = circuitAdapter.lines
            }
        }
    }

    private fun initDeleteButton() {
        binding.menuItemDeleteGate.setOnDragListener { view, event ->
            /**
             * Value comes from [GateViewHolder#bind][com.icxolu.quantumsimulator.items.itemDelegates.GateViewDelegate.ViewHolder.bind]
             */
            val state = event.localState as DragObject
            when (event.action) {
                DragEvent.ACTION_DRAG_STARTED, DragEvent.ACTION_DRAG_LOCATION -> {
                    true
                }
                DragEvent.ACTION_DRAG_ENTERED -> {
                    binding.menuItemDeleteGate.backgroundTintList = ColorStateList.valueOf(getColorFromAttr(R.attr.cErrorContainer))
                    binding.menuItemDeleteGate.iconTint = ColorStateList.valueOf(getColorFromAttr(R.attr.cOnErrorContainer))
                    view.invalidate()
                    true
                }
                DragEvent.ACTION_DROP -> {
                    binding.menuItemDeleteGate.backgroundTintList = ColorStateList.valueOf(getColorFromAttr(R.attr.cSecondaryContainer))
                    binding.menuItemDeleteGate.iconTint = ColorStateList.valueOf(getColorFromAttr(R.attr.cOnSecondaryContainer))
                    view.invalidate()
                    if (state.isGateListAdapter) {
                        gateListAdapter.removeItem(state.holder.adapterPosition)
                    } else {
                        circuitAdapter.replaceItem(state.holder.adapterPosition, IdentityGate())
                    }
                    true

                }
                DragEvent.ACTION_DRAG_EXITED, DragEvent.ACTION_DRAG_ENDED -> {
                    binding.menuItemDeleteGate.backgroundTintList = ColorStateList.valueOf(getColorFromAttr(R.attr.cSecondaryContainer))
                    binding.menuItemDeleteGate.iconTint = ColorStateList.valueOf(getColorFromAttr(R.attr.cOnSecondaryContainer))
                    view.invalidate()
                    true
                }
                else -> false
            }
        }
    }

    private fun initSimulate() {
        binding.btnSimulate.setOnClickListener {
            if (!manyQubits) {
                val intent = Intent(this, ComputeActivity::class.java).apply {
                    putExtra(CIRCUIT_ADAPTER, circuitAdapter)
                }
                startActivity(intent)
            } else {
                CreateManyQubitsDialog.newInstance().show(this.supportFragmentManager, null)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_item_clear_circuit -> {
                circuitAdapter.clear()
                (binding.circuitView.layoutManager as GridLayoutManager).spanCount = circuitAdapter.lines
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * save information of the adapters for e.g. orientation change
     */
    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(CIRCUIT_ADAPTER, circuitAdapter)
        outState.putParcelable(GATE_LIST_ADAPTER, gateListAdapter)
        super.onSaveInstanceState(outState)
    }

    override fun onGateCreated(angle: CharSequence, phase: CharSequence, x: CharSequence, y: CharSequence, z: CharSequence, label: CharSequence, gateColor: Int, gateBottomTint: Int) {
        var angle = BigDecimal(angle)
        var phase = BigDecimal(phase)

        val x = BigDecimal(x)
        val y = BigDecimal(y)
        val z = BigDecimal(z)
        val norm = sqrt(x.pow(2, Complex.MATH_CTX) + y.pow(2, Complex.MATH_CTX) + z.pow(2, Complex.MATH_CTX))

        angle *= BigDecimalMath.pi(Complex.MATH_CTX) / 180
        phase *= BigDecimalMath.pi(Complex.MATH_CTX) / 180

        val matrix = Complex.createFromAngle(phase) * (cos(angle / 2) * IdentityGate.MATRIX - Complex.i * sin(angle / 2) / norm * (x * NotGate.MATRIX + y * YGate.MATRIX + z * ZGate.MATRIX))

        val userGate = UserGate(matrix, label.toString(), gateColor, gateBottomTint)

        gateListAdapter.addItem(userGate)
    }
}
