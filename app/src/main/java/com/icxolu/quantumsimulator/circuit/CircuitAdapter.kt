package com.icxolu.quantumsimulator.circuit

import android.os.Parcel
import com.icxolu.quantumsimulator.items.IItem
import com.icxolu.quantumsimulator.items.StateItem
import com.icxolu.quantumsimulator.items.gates.IdentityGate
import com.icxolu.quantumsimulator.items.itemDelegates.GateViewDelegate
import com.icxolu.quantumsimulator.items.itemDelegates.StateItemDelegate
import com.icxolu.quantumsimulator.recyclerDelegate.AbsDelegationAdapter
import com.icxolu.quantumsimulator.utils.KParcelable
import com.icxolu.quantumsimulator.utils.checkItemsAre
import com.icxolu.quantumsimulator.utils.parcelableCreator
import com.icxolu.quantumsimulator.utils.readArrayListSDK

class CircuitAdapter(
        items: List<IItem>
): AbsDelegationAdapter<IItem>(), KParcelable {
    companion object {
        @JvmField val CREATOR = parcelableCreator(::CircuitAdapter)
    }

    constructor(source: Parcel): this(source.readArrayListSDK<IItem>()!!.checkItemsAre()) {
        lines = source.readInt()
    }
    override val _items: MutableList<IItem> = items.toMutableList()

    var lines: Int = 2
        private set

    val columns: Int
        get() = itemCount / lines

    var areGatesDraggable: Boolean = true
    var areStatesClickable: Boolean = true

    init {
        addDelegate(GateViewDelegate())
        addDelegate(StateItemDelegate())
    }

    fun getColumnList(column: Int): List<IItem> = items.subList(column * lines, (column + 1) * lines)

    fun getGridOfPosition(position: Int) = position % lines to position / lines

    /**
     * Put new Gridlayoutmanager in recyclerview with new lines
     */
    fun addLine(item: IItem) {
        val additionalItems = if (itemCount % lines == 0) itemCount / lines else throw RuntimeException("Wrong item list size")
        addItem(StateItem(), lines)
        (1 until additionalItems)
                .map { lines + it * (lines + 1) }
                .forEach { addItem(item, it) }
        ++lines
    }

    /**
     * Put new Gridlayoutmanager in recyclerview with new lines returned by this function
     */
    fun removeLine(): Boolean {
        if (lines == 1) return false
        val removableItems = if (itemCount % lines == 0) itemCount / lines else throw RuntimeException("Wrong item list size")
        lines--
        (1..removableItems)
                .map { it * lines }
                .forEach { removeItem(it) }
        return true
    }

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeList(items)
        writeInt(lines)
    }

    fun clear() {
        lines = 2
        _items.clear()
        _items.addAll(listOf(StateItem(), StateItem()))
        _items.addAll(List(20) { IdentityGate() })
        notifyDataSetChanged()
    }
}