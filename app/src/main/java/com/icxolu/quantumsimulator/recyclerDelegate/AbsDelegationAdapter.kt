package com.icxolu.quantumsimulator.recyclerDelegate

import androidx.collection.SparseArrayCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup

abstract class AbsDelegationAdapter<T>(
        private val delegates: SparseArrayCompat<AdapterDelegate<T>> = SparseArrayCompat()
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    lateinit var activity: AppCompatActivity
    protected abstract val _items: MutableList<T>
    val items: List<T>
        get() = _items

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val delegate = getDelegateForViewType(viewType) ?: throw RuntimeException()
        return delegate.onCreateViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val delegate = getDelegateForViewType(holder.itemViewType) ?: throw RuntimeException()
        return delegate.onBindViewHolder(this, holder, items, position)
    }

    override fun getItemCount(): Int = _items.size

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        val delegate = getDelegateForViewType(holder.itemViewType) ?: throw RuntimeException()
        return delegate.onViewRecycled(holder)
    }

    override fun onFailedToRecycleView(holder: RecyclerView.ViewHolder): Boolean {
        val delegate = getDelegateForViewType(holder.itemViewType) ?: throw RuntimeException()
        return delegate.onFailedToRecycleView(holder)
    }

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        val delegate = getDelegateForViewType(holder.itemViewType) ?: throw RuntimeException()
        return delegate.onViewAttachedToWindow(holder)
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        val delegate = getDelegateForViewType(holder.itemViewType) ?: throw RuntimeException()
        return delegate.onViewDetachedFromWindow(holder)
    }

    override fun getItemViewType(position: Int): Int {
        for (i in 0 until delegates.size()) {
            if (delegates.valueAt(i).isForViewType(_items[position])) return delegates.keyAt(i)
        }
        throw RuntimeException()
    }

    open fun addItem(item: T, position: Int = _items.size - 1) {
        _items.add(position, item)
        notifyItemInserted(position)
    }

    open fun addItems(items: List<T>, position: Int = this._items.size) {
        this._items.addAll(position, items)
        notifyItemRangeInserted(position, items.size)
    }

    open fun removeItem(position: Int = _items.size - 1): T = _items.removeAt(position).also {
        notifyItemRemoved(position)
    }

    open fun replaceItem(position: Int, newItem: T) {
        _items[position] = newItem
        notifyItemChanged(position)
    }

    private fun getDelegateForViewType(viewType: Int): AdapterDelegate<T>? {
        return delegates[viewType]
    }

    fun addDelegate(delegate: AdapterDelegate<T>) {
        delegate.adapter = this
        delegates.put(delegate.layoutRes, delegate)
    }
}