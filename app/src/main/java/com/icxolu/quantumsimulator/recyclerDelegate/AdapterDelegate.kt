package com.icxolu.quantumsimulator.recyclerDelegate

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup

abstract class AdapterDelegate<T> {
    abstract val layoutRes: Int
    lateinit var adapter: AbsDelegationAdapter<T>
    val activity: AppCompatActivity
        get() = adapter.activity

    abstract fun isForViewType(item: T): Boolean
    abstract fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder
    abstract fun onBindViewHolder(adapter: AbsDelegationAdapter<T>, holder: RecyclerView.ViewHolder, items: List<T>, position: Int)
    fun onViewRecycled(@Suppress("UNUSED_PARAMETER") holder: RecyclerView.ViewHolder) {}
    fun onFailedToRecycleView(@Suppress("UNUSED_PARAMETER") holder: RecyclerView.ViewHolder): Boolean = false
    fun onViewAttachedToWindow(@Suppress("UNUSED_PARAMETER") holder: RecyclerView.ViewHolder) {}
    fun onViewDetachedFromWindow(@Suppress("UNUSED_PARAMETER") holder: RecyclerView.ViewHolder) {}
}