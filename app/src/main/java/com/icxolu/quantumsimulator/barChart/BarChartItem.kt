package com.icxolu.quantumsimulator.barChart

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View

import kotlin.properties.Delegates
import kotlin.reflect.KProperty

import com.icxolu.quantumsimulator.R
import java.util.*


class BarChartItem(context: Context, attrs: AttributeSet) : View(context, attrs) {

    var data: Float by Delegates.observable(0f, this::reDraw)
    var stateLabel: String by Delegates.observable("", this::reDraw)
    var lastItem: Boolean by Delegates.observable(false, this::reDraw)
    var barWidth: Float by Delegates.observable(0f, this::reDraw)
    var barDistance: Float by Delegates.observable(0f, this::reDraw)
    var labelSize: Float by Delegates.observable(0f, this::reDraw)
    var barColor: Int by Delegates.observable(0, this::reDraw)
    var gridColor: Int by Delegates.observable(0, this::reDraw)
    var zeroLineColor: Int by Delegates.observable(0, this::reDraw)
    var labelColor: Int by Delegates.observable(0, this::reDraw)

    private var mHeight = 0f
    private var mWidth = 0f

    private val paddingHeaderFooter = 30f
    private val labelOffset = 15f
    private val cornerRadius = 10f
    private val strokeWidth = 3f
    private val posGridLabels = listOf(0f, 0.25f, 0.5f, 0.75f, 1f)

    private val bounds = Rect()

    private val stateLabelWidth : Int
        get() {
            labelPaint.getTextBounds(stateLabel, 0, stateLabel.length, bounds)
            return bounds.width()
    }
    private val gridLabelHeight : Int
        get() {
            val gridLabelAtZero = "%.2f".format(Locale.ENGLISH, posGridLabels[0])
            labelPaint.getTextBounds(gridLabelAtZero, 0, gridLabelAtZero.length, bounds)
            return bounds.height()
        }

    private val header get() = paddingHeaderFooter
    private val footer get() = stateLabelWidth + paddingHeaderFooter
    private val body get() = mWidth - footer - header
    private val extraHeightLastItem : Float get() = if (lastItem) gridLabelHeight.toFloat() + labelOffset else 0f

    init {
        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.BarChartItem,0, 0)
        try {
            barWidth = a.getDimension(R.styleable.BarChartItem_barWidth, barWidth)
            barDistance = a.getDimension(R.styleable.BarChartItem_barDistance, barDistance)
            labelSize = a.getDimension(R.styleable.BarChartItem_labelSize, labelSize)
            barColor = a.getColor(R.styleable.BarChartItem_barColor, barColor)
            gridColor = a.getColor(R.styleable.BarChartItem_gridColor, gridColor)
            zeroLineColor = a.getColor(R.styleable.BarChartItem_zeroLineColor, zeroLineColor)
            labelColor = a.getColor(R.styleable.BarChartItem_labelColor, labelColor)
        } finally {
            a.recycle()
        }
    }

    private var barPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = barColor
    }
    private var labelPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = labelColor
        textAlign = Paint.Align.CENTER
        textSize = labelSize
    }
    private var zeroLinePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = zeroLineColor
        strokeWidth = strokeWidth
    }
    private var gridPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = gridColor
        strokeWidth = strokeWidth
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        
        mWidth = w - (paddingStart + paddingEnd).toFloat()
        mHeight = h - (paddingBottom + paddingTop).toFloat()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val desiredWidth = 3000
        val desiredHeight = (barWidth + barDistance + extraHeightLastItem).toInt()

        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)

        val width = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> desiredWidth.coerceAtMost(widthSize)
            else -> desiredWidth
        }
        val height = when (heightMode) {
            MeasureSpec.EXACTLY -> heightSize
            MeasureSpec.AT_MOST -> desiredHeight.coerceAtMost(heightSize)
            else -> desiredHeight
        }

        setMeasuredDimension(width, height)
    }

    override fun onDraw(canvas: Canvas) {

        val mHeightNew = mHeight - extraHeightLastItem

        val barBottom = mHeightNew / 2f - barWidth / 2f
        val barTop = barBottom + barWidth
        val barLeft = footer
        val barRight = footer + data * body
        val barLength = barRight - barLeft

        val xPosStateLabel = footer - stateLabelWidth / 2f - labelOffset
        val yPosStateLabel = mHeightNew / 2f + gridLabelHeight / 2f
        val yPosDataLabel = yPosStateLabel
        val yPosGridLabels = mHeightNew + gridLabelHeight / 2f + labelOffset

        val dataLabel = "%.3f".format(Locale.ENGLISH, data)

        // Draw grid lines
        var x: Float
        posGridLabels.forEach {
            x = footer + it * body
            canvas.drawLine(x, 0f, x, mHeightNew, gridPaint)
        }

        // Draw bar
        if (data > 1E-4) {
            canvas.drawRect(barLeft, barTop, barRight - cornerRadius, barBottom, barPaint)
            canvas.drawRoundRect(barRight - 2 * cornerRadius, barTop, barRight, barBottom, cornerRadius, cornerRadius, barPaint)
        }

        // Draw zeroline
        canvas.drawLine(footer, 0f, footer, mHeightNew, zeroLinePaint)

        // Draw data labels
        if (data > 1E-4) {
            labelPaint.getTextBounds(dataLabel, 0, dataLabel.length, bounds)
            val dataLabelWidth = bounds.width()
            val xPosDataLabel = if (dataLabelWidth > barLength) {
                barRight + dataLabelWidth / 2f + labelOffset
            } else {
                barRight - dataLabelWidth / 2f - labelOffset
            }
            canvas.drawText(dataLabel, xPosDataLabel, yPosDataLabel, labelPaint)
        }

        // Draw state labels (y-axis)
        canvas.drawText(stateLabel, xPosStateLabel, yPosStateLabel, labelPaint)

        // Draw grid labels (x-axis)
        if (lastItem) {
            posGridLabels.forEach {
                x = footer + it * body
                canvas.drawText("%.2f".format(Locale.ENGLISH, it), x, yPosGridLabels, labelPaint)
            }
        }
    }

    @Suppress("UNUSED_PARAMETER")
    private fun <T> reDraw(property: KProperty<*>, oldValue: T, newValue: T) {
        if (oldValue != newValue) {
            invalidate()
        }
    }
}
