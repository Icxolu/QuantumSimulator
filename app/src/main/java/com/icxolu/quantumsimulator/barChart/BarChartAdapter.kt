package com.icxolu.quantumsimulator.barChart

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.icxolu.quantumsimulator.R
import com.icxolu.quantumsimulator.utils.using

class BarChartAdapter(
        private val data: ArrayList<Float> = arrayListOf(),
        private val labels: ArrayList<String> = arrayListOf()
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    lateinit var activity: AppCompatActivity

    init {
        if (data.size != labels.size) throw IllegalStateException("size of data and labels does not match")
    }

    class MyViewHolder(val barChartItem: BarChartItem) : RecyclerView.ViewHolder(barChartItem)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val barChartItem = LayoutInflater.from(parent.context)
                .inflate(R.layout.bar_chart_item, parent, false) as BarChartItem
        return MyViewHolder(barChartItem)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder !is MyViewHolder) throw IllegalStateException("wrong viewHolder")
        viewHolder.barChartItem.data = data[position]
        viewHolder.barChartItem.stateLabel = labels[position]
        viewHolder.barChartItem.lastItem = position == this.itemCount - 1
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun set(data: List<Float>, labels: List<String>) {
        if (data.size != labels.size) throw IllegalStateException("size of data and labels does not match")

        using(this.data) {
            clear()
            addAll(data)
        }
        using(this.labels) {
            clear()
            addAll(labels)
        }
        notifyDataSetChanged()
    }
}