package com.icxolu.quantumsimulator.gateList.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.icxolu.quantumsimulator.R
import com.icxolu.quantumsimulator.databinding.CreateGateDialogBinding

class CreateGateDialog: DialogFragment() {
    companion object {
        fun newInstance() = CreateGateDialog()
    }

    private lateinit var listener: CreateGateDialogListener
    private val adapter by lazy { StyleListAdapter(requireContext()) }

    interface CreateGateDialogListener {
        fun onGateCreated(angle: CharSequence, phase: CharSequence, x: CharSequence,
                          y: CharSequence, z: CharSequence, label: CharSequence, gateColor: Int,
                          gateBottomTint: Int)
    }

    /**
     * Check if attached activity implements listener for interaction
     */
    override fun onAttach(context: Context) {
        super.onAttach(context)

        listener = try {
            context as CreateGateDialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException("$context must implement StylePickerDialogListener")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val binding = CreateGateDialogBinding.inflate(layoutInflater, null, false)

        binding.colorList.adapter = adapter
        binding.colorList.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

        binding.gateLabel.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                binding.preview.text = s.toString()
            }
        })

        // Create an alert dialog with a custom layout, we set null to the listener because we
        // override it in onResume, so the dialog does not dismiss then we click the buttons
        val builder = MaterialAlertDialogBuilder(requireActivity())
        builder.setTitle("Create Gate")
                .setView(binding.root)
                .setPositiveButton("Create", null)
                .setNegativeButton("Cancel", null)

        return builder.create()
    }

    override fun onResume() {
        super.onResume()

        val dialog = dialog as AlertDialog
        adapter.dialog = dialog

        val cancelBtn = dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
        val createBtn = dialog.getButton(AlertDialog.BUTTON_POSITIVE)

        cancelBtn.setOnClickListener { dismiss() }
        createBtn.setOnClickListener {
            val label = dialog.findViewById<TextInputEditText>(R.id.gateLabel)
            val angle = dialog.findViewById<TextInputEditText>(R.id.angle)
            val phase = dialog.findViewById<TextInputEditText>(R.id.phase)
            val x = dialog.findViewById<TextInputEditText>(R.id.x_cord_axis)
            val y = dialog.findViewById<TextInputEditText>(R.id.y_cord_axis)
            val z = dialog.findViewById<TextInputEditText>(R.id.z_cord_axis)

            var error = false

            if (label?.text.isNullOrBlank()) {
                label?.error = "invalid"
                error = true
            }

            if (angle?.text.isNullOrBlank()) {
                angle?.error = "invalid"
                error = true
            }
            if (x?.text.isNullOrBlank()) {
                x?.error = "invalid"
                error=true
            }
            if (y?.text.isNullOrBlank()) {
                y?.error = "invalid"
                error = true
            }
            if (z?.text.isNullOrBlank()) {
                z?.error = "invalid"
                error = true
            }
            if (!error) {
                val phase = if (phase?.text.isNullOrBlank()) "0" else phase?.text
                listener.onGateCreated(angle?.text!!, phase!!, x?.text!!, y?.text!!, z?.text!!,
                        label?.text!!, adapter.selectedItem.gateColor, adapter.selectedItem.gateBottomTint)
                dismiss()
            }
        }
    }
}