package com.icxolu.quantumsimulator.gateList

import android.os.Parcel
import com.icxolu.quantumsimulator.items.IItem
import com.icxolu.quantumsimulator.items.itemDelegates.AddViewDelegate
import com.icxolu.quantumsimulator.items.itemDelegates.GateViewDelegate
import com.icxolu.quantumsimulator.recyclerDelegate.AbsDelegationAdapter
import com.icxolu.quantumsimulator.utils.KParcelable
import com.icxolu.quantumsimulator.utils.checkItemsAre
import com.icxolu.quantumsimulator.utils.parcelableCreator
import com.icxolu.quantumsimulator.utils.readArrayListSDK

class GateListAdapter(
        items: List<IItem>
): AbsDelegationAdapter<IItem>(), KParcelable {
    constructor(source: Parcel): this(source.readArrayListSDK<IItem>()!!.checkItemsAre())

    companion object {
        @JvmField val CREATOR = parcelableCreator(::GateListAdapter)
    }

    override val _items: MutableList<IItem> = items.toMutableList()

    init {
        addDelegate(GateViewDelegate())
        addDelegate(AddViewDelegate())
    }

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeList(items)
    }
}