package com.icxolu.quantumsimulator.gateList.dialog

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.annotation.ColorInt
import androidx.recyclerview.widget.RecyclerView

import com.icxolu.quantumsimulator.R
import com.icxolu.quantumsimulator.databinding.CreateGateDialogBinding
import com.icxolu.quantumsimulator.databinding.StyleItemBinding
import com.icxolu.quantumsimulator.utils.UserGateView
import com.icxolu.quantumsimulator.utils.getColorFromAttr

class StyleListAdapter(private val context: Context): RecyclerView.Adapter<StyleListAdapter.ViewHolder>() {

    private val gateColors = intArrayOf(
        context.getColorFromAttr(R.attr.cCustomColor2Container),
        context.getColorFromAttr(R.attr.cCustomColor1Container),
        context.getColorFromAttr(R.attr.cCustomColor3Container),
        context.getColorFromAttr(R.attr.cCustomColor4Container),
        context.getColorFromAttr(R.attr.cCustomColor5Container),
        context.getColorFromAttr(R.attr.cCustomColor6Container),
        context.getColorFromAttr(R.attr.cCustomColor7Container),
        context.getColorFromAttr(R.attr.cCustomColor8Container)
    )
    private val gateBottomTints = intArrayOf(
        context.getColorFromAttr(R.attr.cCustomColor2Container),
        context.getColorFromAttr(R.attr.cCustomColor1Container),
        context.getColorFromAttr(R.attr.cCustomColor3Container),
        context.getColorFromAttr(R.attr.cCustomColor4Container),
        context.getColorFromAttr(R.attr.cCustomColor5Container),
        context.getColorFromAttr(R.attr.cCustomColor6Container),
        context.getColorFromAttr(R.attr.cCustomColor7Container),
        context.getColorFromAttr(R.attr.cCustomColor8Container)
    )

    lateinit var dialog: Dialog
    lateinit var selectedItem: ViewHolder
        private set

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.style_item, parent, false))
    }

    override fun getItemCount(): Int = gateColors.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val bindingStyleItem = StyleItemBinding.bind(itemView)
        private val bindingGateDialog = CreateGateDialogBinding.inflate(LayoutInflater.from(context))
        private val gate: UserGateView = bindingStyleItem.gate
        private val preview: UserGateView = bindingGateDialog.preview
        private var isSelected: Boolean = true

        @get:ColorInt val gateColor: Int get() = gate.gateColor
        @get:ColorInt val gateBottomTint: Int get() = gate.gateBottomTint

        fun bind(position: Int) {
            gate.text = ""
            gate.gateColor = gateColors[position]
            isSelected = preview.gateColor == gate.gateColor
            if (isSelected) selectedItem = this

            gate.setOnClickListener {
                selectedItem.isSelected = false
                isSelected = true
                selectedItem = this
                with(dialog.findViewById<UserGateView>(R.id.preview)) {
                    gateColor = gate.gateColor
                    gateBottomTint = gate.gateBottomTint
                }
            }
        }
    }
}