package com.icxolu.quantumsimulator.utils

import android.text.Spannable
import android.text.TextPaint
import android.text.style.RelativeSizeSpan
import android.text.style.SubscriptSpan
import android.text.style.SuperscriptSpan

/**
 * custom subscript span to adjust the shift distance
 * lower subscript by 14%
 * refer to https://en.wikipedia.org/wiki/Subscript_and_superscript
 */
class SubSpan(private val baselineShift: Float = 0.14f): SubscriptSpan() {
    override fun updateDrawState(tp: TextPaint) { tp.baselineShift -= (tp.ascent() * baselineShift).toInt() }
    override fun updateMeasureState(tp: TextPaint) { tp.baselineShift -= (tp.ascent() * baselineShift).toInt() }
}

/**
 * custom superscript span to adjust the shift distance
 * increate subscript by 50%
 * refer to https://en.wikipedia.org/wiki/Subscript_and_superscript
 */
class SupSpan(private val baselineShift: Float = 0.50f): SuperscriptSpan() {
    override fun updateDrawState(tp: TextPaint) { tp.baselineShift += (tp.ascent() * baselineShift).toInt() }
    override fun updateMeasureState(tp: TextPaint) { tp.baselineShift += (tp.ascent() * baselineShift).toInt() }
}

/**
 * convenience method to add a [SubSpan] and an [RelativeSizeSpan] to a [Spannable]
 * @param start index of first character to apply the span on
 * @param end index of character to stop applying the span on
 * @param proportion relative font scale, default is 58%
 * @param baselineShift relative baseline shift, default is 14%
 * @param flags see e.g. [Spannable.SPAN_EXCLUSIVE_EXCLUSIVE]
 */
fun Spannable.subSpan(start: Int, end: Int, proportion: Float = 0.58f, baselineShift: Float = 0.14f, flags: Int = Spannable.SPAN_EXCLUSIVE_EXCLUSIVE) {
    setSpan(SubSpan(baselineShift), start, end, flags)
    setSpan(RelativeSizeSpan(proportion), start, end, flags)
}

/**
 * convenience method to add a [SupSpan] and an [RelativeSizeSpan] to a [Spannable]
 * @param start index of first character to apply the span on
 * @param end index of character to stop applying the span on
 * @param proportion relative font scale, default is 58%
 * @param baselineShift relative baseline shift, default is 33%
 * @param flags see e.g. [Spannable.SPAN_EXCLUSIVE_EXCLUSIVE]
 */
fun Spannable.supSpan(start: Int, end: Int, proportion: Float = 0.58f, baselineShift: Float = 0.50f, flags: Int = Spannable.SPAN_EXCLUSIVE_EXCLUSIVE) {
    setSpan(SupSpan(baselineShift), start, end, flags)
    setSpan(RelativeSizeSpan(proportion), start, end, flags)
}