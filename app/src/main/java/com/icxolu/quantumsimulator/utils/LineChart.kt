package com.icxolu.quantumsimulator.utils

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View

import kotlin.properties.Delegates

import com.icxolu.quantumsimulator.R

class LineChart(context: Context, attrs: AttributeSet): View(context, attrs) {

    var data by Delegates.observable(listOf<Float>()) { _, old, new ->
        if (old != new) {
            requestLayout()
            invalidate()
        }
    }
    var label by Delegates.observable(-1) { _, old, new ->
        if (old != new) {
            requestLayout()
            invalidate()
        }
    }
    var manyQubits by Delegates.observable(false) { _, old, new ->
        if (old != new) {
            requestLayout()
            invalidate()
        }
    }

    private var textPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = context.getColorFromAttr(android.R.attr.textColorPrimary)
        textAlign = Paint.Align.CENTER
        textSize = 30f
    }
    private var labelPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = context.getColorFromAttr(android.R.attr.textColorPrimary)
        textAlign = Paint.Align.CENTER
        textSize = 30f
    }
    private var ticksPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = context.getColorFromAttr(android.R.attr.textColorPrimary)
        textAlign = Paint.Align.LEFT
        textSize = 30f
    }
    private var zeroLinePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = context.getColorFromAttr(R.attr.cBarZeroLine)
        strokeWidth = 3f
    }
    private var gridPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = context.getColorFromAttr(R.attr.cBarGrid)
        strokeWidth = 3f
    }
    private var linePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = context.getColorFromAttr(R.attr.cBars)
        strokeWidth = 3f
    }

    private val textOffset = 10f
    private val labelOffset = 17f
    private val textHeight = textPaint.textSize
    private val labelHeight = labelPaint.textSize
    private val ticksBounds = Rect()

    private var mHeight = 0f
    private var mWidth = 0f

    private var bufferedImg = Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888)
    private var mCanvas = Canvas(bufferedImg)

    private val header = textHeight + textOffset
    private val footer = labelHeight + labelOffset
    private val zeroLine get() = mHeight - footer
    private val body get() = zeroLine - header

    init {
        ticksPaint.getTextBounds("0.00", 0, "0.00".length, ticksBounds)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        mWidth = w - (paddingStart + paddingEnd).toFloat()
        mHeight = h - (paddingBottom + paddingTop).toFloat()
        bufferedImg = Bitmap.createBitmap(mWidth.toInt(), mHeight.toInt(), Bitmap.Config.ARGB_8888)
        mCanvas = Canvas(bufferedImg)
    }

    override fun onDraw(canvas: Canvas) {

        val xOffset = ticksBounds.width() + 15f
        val scale = (mWidth-xOffset)/(data.size-1)

        listOf("0.00", "0.25", "0.50", "0.75", "1.00").forEach {
            val y = zeroLine - it.toFloat() * body
            mCanvas.drawText(it, 0f, y+ticksBounds.height()/2, ticksPaint)
            mCanvas.drawLine(xOffset, y, mWidth, y, gridPaint)
        }
        mCanvas.drawLine(xOffset, zeroLine, mWidth, zeroLine, zeroLinePaint)

        for (i in 1 until data.size) {
            mCanvas.drawLine(scale*(i-1)+xOffset, zeroLine - data[i-1] * body, scale*i+xOffset, zeroLine - data[i] * body, linePaint)
        }

        if (label != -1) {
            canvas.drawText(data[label].toString(), scale*label + xOffset, zeroLine - data[label] * body - textOffset, textPaint)
            when (manyQubits) {
                true -> canvas.drawText("|$label⟩", scale*label + xOffset, mHeight - 7f, labelPaint)
                false -> canvas.drawText("|"+ deciToBinStr(label, (Math.log(data.size.toDouble())/Math.log(2.0)).toInt()) +"⟩", label*scale + xOffset, mHeight - 7f, labelPaint)
            }
        }

        canvas.drawBitmap(bufferedImg, 0f, 0f, null)
    }


}