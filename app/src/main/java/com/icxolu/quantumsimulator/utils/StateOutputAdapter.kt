package com.icxolu.quantumsimulator.utils

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.icxolu.quantumsimulator.R

class StateOutputAdapter(
        private val data: ArrayList<String> = arrayListOf(),
        val manyQubits: Boolean,
        val circuitLines: Int
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    lateinit var activity: AppCompatActivity

    class MyViewHolder(val textViewItem: TextView) : RecyclerView.ViewHolder(textViewItem)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val textViewItem = LayoutInflater.from(parent.context)
                .inflate(R.layout.text_view_item, parent, false) as TextView
        textViewItem.setOnClickListener(ManyQubitsClickListener(this))
        return MyViewHolder(textViewItem)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder !is MyViewHolder) throw IllegalStateException("wrong viewHolder")
        viewHolder.textViewItem.text = data[position]
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun set(data: ArrayList<String>) {

        using(this.data) {
            clear()
            addAll(data)
        }
        notifyDataSetChanged()
    }
}