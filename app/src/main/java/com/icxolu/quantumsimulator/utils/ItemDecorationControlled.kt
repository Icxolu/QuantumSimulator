package com.icxolu.quantumsimulator.utils

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.icxolu.quantumsimulator.circuit.CircuitAdapter
import com.icxolu.quantumsimulator.items.gates.ControlledGate

class ItemDecorationControlled: RecyclerView.ItemDecoration() {
    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        c.save()
        val adapter: CircuitAdapter = if (parent.adapter is CircuitAdapter) parent.adapter as CircuitAdapter else throw RuntimeException()

        for (i in 0 until parent.childCount) {
            val child = parent.getChildAt(i)
            val adapterPos = parent.getChildAdapterPosition(child)

            val (line, column) = adapter.getGridOfPosition(adapterPos)

            if (adapter.lines > 1) {
                if (adapter.getColumnList(column).any { it is ControlledGate && it.isVisible }) {
                    drawControlLine(c, child, adapter, line)
                }
            }
        }
        c.restore()
    }
}

fun drawControlLine(
        c: Canvas,
        view: View,
        adapter: CircuitAdapter,
        line: Int,
        paint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = Color.BLACK
            strokeWidth = view.context.convertDpToPx(1).toFloat()
            style = Paint.Style.STROKE
        }
) = using(view) {
    when (line) {
        0 -> {
            c.drawLine(
                    left + measuredWidth / 2,
                    top + measuredHeight / 2,
                    left + measuredWidth / 2,
                    bottom,
                    paint
            )
        }
        adapter.lines - 1 -> {
            c.drawLine(
                    left + measuredWidth / 2,
                    top,
                    left + measuredWidth / 2,
                    bottom - measuredHeight / 2,
                    paint
            )
        }
        else -> {
            c.drawLine(
                    left + measuredWidth / 2,
                    top,
                    left + measuredWidth / 2,
                    bottom,
                    paint
            )
        }
    }
}