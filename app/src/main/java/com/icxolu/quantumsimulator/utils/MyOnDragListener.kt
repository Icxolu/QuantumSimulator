package com.icxolu.quantumsimulator.utils

import android.content.ClipDescription
import android.graphics.PorterDuff
import android.widget.Button
import android.view.DragEvent
import android.view.View

import androidx.recyclerview.widget.RecyclerView

import com.icxolu.quantumsimulator.MainActivity
import com.icxolu.quantumsimulator.R
import com.icxolu.quantumsimulator.circuit.CircuitAdapter
import com.icxolu.quantumsimulator.items.IItem
import com.icxolu.quantumsimulator.items.gates.ControlledGate
import com.icxolu.quantumsimulator.items.gates.IdentityGate
import com.icxolu.quantumsimulator.items.itemDelegates.DragObject
import com.icxolu.quantumsimulator.recyclerDelegate.AbsDelegationAdapter

class MyOnDragListener(private val adapter: AbsDelegationAdapter<IItem>, private val holder: RecyclerView.ViewHolder): View.OnDragListener {

    override fun onDrag(view: View, event: DragEvent): Boolean {
        /**
         * Value comes from [GateViewHolder#bind][com.icxolu.quantumsimulator.items.itemDelegates.GateViewDelegate.ViewHolder.bind]
         */
        val state = event.localState as DragObject
        val binding = (adapter.activity as MainActivity).binding

        return when (event.action) {
            DragEvent.ACTION_DRAG_STARTED -> {
                if (event.clipDescription.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                    view.background.clearColorFilter()
                    view.invalidate()
                    true
                } else {
                    false
                }
            }
            DragEvent.ACTION_DRAG_ENTERED -> {
                view.background.setColorFilter(adapter.activity.getColorFromAttr(R.attr.cConfirmContainer), PorterDuff.Mode.ADD)
                view.invalidate()
                true
            }
            DragEvent.ACTION_DRAG_LOCATION -> {
                true
            }
            DragEvent.ACTION_DRAG_EXITED -> {
                view.background.clearColorFilter()
                view.invalidate()
                true
            }
            DragEvent.ACTION_DROP -> {
                view.background.clearColorFilter()
                view.invalidate()

                // Replace dragged gate from circuit by IdentityGate
                if (!state.isGateListAdapter) {
                    adapter.replaceItem(state.holder.adapterPosition, IdentityGate())
                }

                // Put a new controlled gate in circuit if it comes from gatelist
                if (state.isGateListAdapter && state.item is ControlledGate) {
                    adapter.replaceItem(holder.adapterPosition, ControlledGate())
                } else {
                    adapter.replaceItem(holder.adapterPosition, state.item)
                }
                (state.item as? ControlledGate)?.isVisible = true

                if (adapter is CircuitAdapter) {
                    (adapter.activity.application as Globals).rowsWithGates.add(holder.adapterPosition / adapter.lines)
                }

                binding.menuItemDeleteGate.visibility = Button.GONE
                true
            }
            DragEvent.ACTION_DRAG_ENDED -> {
                view.background.clearColorFilter()
                view.invalidate()

                if (!event.result && !state.isGateListAdapter) {
                    if (state.item is ControlledGate) {
                        state.holder.img.visibility = View.VISIBLE
                        state.item.isVisible = true
                    } else {
                        state.holder.gate.visibility = View.VISIBLE
                    }
                }

                binding.menuItemDeleteGate.visibility = Button.GONE

                true
            }
            else -> false
        }
    }
}