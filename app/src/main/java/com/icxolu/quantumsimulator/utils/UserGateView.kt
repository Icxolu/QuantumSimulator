package com.icxolu.quantumsimulator.utils

import android.content.Context
import android.graphics.*
import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import android.text.*
import android.util.AttributeSet
import android.view.View
import androidx.core.graphics.createBitmap
import com.icxolu.quantumsimulator.R
import kotlin.math.min
import kotlin.math.roundToInt
import kotlin.properties.Delegates
import kotlin.reflect.KProperty


class UserGateView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
): View(context, attrs, defStyleAttr) {
    companion object {
        private const val LONG_SHADOW_COLOR = 0x33212121
        private const val WHITE_20 = 0x33FFFFFF
        private const val WHITE_40 = 0x66FFFFFF
        private const val GREY_900_10 = 0x1AF5F5F5
        private const val BLUE_GREY_900_20 = 0x33263238
        private const val TEAL_500 = 0xFF009688.toInt()
        private const val TEXT_COLOR = 0xFFF5F5F5.toInt()
    }

    private val editable by lazy { Editable.Factory.getInstance().newEditable("") }
    private val layout by lazy {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
            DynamicLayout.Builder.obtain(editable, textPaint, Int.MAX_VALUE)
                    .setAlignment(Layout.Alignment.ALIGN_NORMAL)
                    .setLineSpacing(0f, 1f)
                    .setIncludePad(false)
                    .build()
        else {
            @Suppress("DEPRECATION")
            DynamicLayout(editable, textPaint, Int.MAX_VALUE, Layout.Alignment.ALIGN_NORMAL, 1f, 0f, false)
        }
    }

    var text: String
        get() = editable.toString()
        set(value) {
            editable.clear()
            editable.append(value)
            requestLayout()
            invalidate()
        }

    var textSize: Int by Delegates.observable(context.convertDpToPx(27), this::refreshLayoutAndReDraw)

    var textColor: Int by Delegates.observable(TEXT_COLOR, this::reDraw)
    var textTopTint: Int by Delegates.observable(WHITE_40, this::reDraw)
    var textBottomTint: Int by Delegates.observable(GREY_900_10, this::reDraw)
    var gateColor: Int by Delegates.observable(TEAL_500, this::reDraw)
    var gateTopTint: Int by Delegates.observable(WHITE_20, this::reDraw)
    var gateBottomTint: Int by Delegates.observable(BLUE_GREY_900_20, this::reDraw)
    var cornerRadius: Int by Delegates.observable(context.convertDpToPx(3), this::reDraw)

    init {
        val ta = context.theme.obtainStyledAttributes(attrs, R.styleable.UserGateView, defStyleAttr, R.style.UsergateDefaultStyle)
        try {
            text = ta.getString(R.styleable.UserGateView_text) ?: ""
            textSize = ta.getDimensionPixelSize(R.styleable.UserGateView_textSize, textSize)
            textColor = context.getColorFromAttr(android.R.attr.textColorPrimary)  // ta.getColor(R.styleable.UserGateView_textColor, textColor)
            textTopTint = ta.getColor(R.styleable.UserGateView_textTopTint, textTopTint)
            textBottomTint = ta.getColor(R.styleable.UserGateView_textBottomTint, textBottomTint)
            gateColor = ta.getColor(R.styleable.UserGateView_gateColor, gateColor)
            gateTopTint = ta.getColor(R.styleable.UserGateView_gateTopTint, gateTopTint)
            gateBottomTint = ta.getColor(R.styleable.UserGateView_gateBottomTint, gateBottomTint)
            cornerRadius = ta.getDimensionPixelSize(R.styleable.UserGateView_cornerRadius, cornerRadius)
        } finally {
            ta.recycle()
        }
    }

    private val textPaint by lazy { TextPaint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.WHITE
        textSize = this@UserGateView.textSize.toFloat()
        textAlign = Paint.Align.LEFT
    } }

    private val colorPaint by lazy { Paint(Paint.ANTI_ALIAS_FLAG) }
    private val shaderPaint by lazy { Paint(Paint.ANTI_ALIAS_FLAG) }
    private val dstOutMode by lazy { PorterDuffXfermode(PorterDuff.Mode.DST_OUT) }

    //I have no idea why modifying a bitmap after it was drawn on the canvas effects the drawing, but
    //that's why we need 3 bitmaps
    private val shadowBitmap: Bitmap by lazy { createBitmap(nWidth, nHeight) }
    private val textTopTintBitmap: Bitmap by lazy { createBitmap(nWidth, nHeight) }
    private val textBottomTintBitmap: Bitmap by lazy { createBitmap(nWidth, nHeight) }
    private val shadowCanvas by lazy { Canvas(shadowBitmap) }
    private val textTopTintCanvas by lazy { Canvas(textTopTintBitmap) }
    private val textBottomTintCanvas by lazy { Canvas(textBottomTintBitmap) }

    /*private val linearShader by lazy {
        val x = min(nWidth / 2, nHeight / 2)
        LinearGradient(nWidth / 2f, nHeight / 2f, nWidth / 2f + x, nHeight / 2f  + x, LONG_SHADOW_COLOR, Color.TRANSPARENT, Shader.TileMode.CLAMP)
    }
    private val radialShader by lazy { RadialGradient(0f, 0f, width / 2f, WHITE_20, Color.TRANSPARENT, Shader.TileMode.CLAMP) }*/

    private val path by lazy { Path() }
    private val path2 by lazy { Path() }

    private val nWidth: Int get() = width - paddingStart - paddingEnd
    private val nHeight: Int get() = height - paddingTop - paddingBottom

    /*private fun updateShadow() = shadowBitmap.applyCanvas (shadowCanvas) {
        it.eraseColor(Color.TRANSPARENT)

        //draw initial text
        save()
        translate(nWidth / 2f - layout.getLineMax(0) / 2f, nHeight / 2f - layout.height / 2)
        layout.draw(this)
        restore()

        val src = Rect()
        val dst = Rect()

        val w = it.width
        val h = it.height

        src.left = 0
        src.right = w

        //generate shadow
        (0..h).forEach {i ->
            src.top = i
            src.bottom = i + 1

            dst.left = 1
            dst.right = 1 + w
            dst.top = i + 1
            dst.bottom = i + 2

            drawBitmap(it, src, dst, null)
        }

        //apply gradient to shadow
        shaderPaint.shader = linearShader
        shaderPaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        drawRect(0f, 0f, w.toFloat(), h.toFloat(), shaderPaint)
        shaderPaint.xfermode = null
    }*/

    /**
     * called after the view is layed out, so the final dimensions are new known and we can generate
     * the long shadow of the text.
     */
    /*override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        updateShadow()
    }*/

    override fun onDraw(canvas: Canvas) {
        canvas.save()
        canvas.translate(paddingStart.toFloat(), paddingTop.toFloat())

        //Draw backgrounds
        path.rewind()
        path.addRoundRect(0, 0, nWidth, nHeight, cornerRadius, cornerRadius, Path.Direction.CCW)
        colorPaint.color = gateColor
        canvas.drawPath(path, colorPaint)

        //draw bg top tint
        /*path.offset(0f, context.convertDpToPx(1).toFloat(), path2)
        path -= path2
        colorPaint.color = gateTopTint
        canvas.drawPath(path, colorPaint)*/

        //draw bg bottom tint
        /*path.rewind()
        path.addRoundRect(0, 0, nWidth, nHeight, cornerRadius, cornerRadius, Path.Direction.CCW)
        path.offset(0f, -context.convertDpToPx(1).toFloat(), path2)
        path -= path2
        colorPaint.color = gateBottomTint
        canvas.drawPath(path, colorPaint)*/

        //draw long shadow
        /*canvas.save()
        path.rewind()
        path.addRoundRect(0, 0, nWidth, nHeight, cornerRadius, cornerRadius, Path.Direction.CCW)
        canvas.clipPath(path)
        canvas.drawBitmap(shadowBitmap, 0f, 0f, null)
        canvas.restore()*/

        //draw text above shadow
        canvas.save()
        canvas.translate(nWidth / 2f - layout.getLineMax(0) / 2f, nHeight / 2f - layout.height / 2f)
        textPaint.color = textColor
        layout.draw(canvas)
        canvas.restore()

        //draw top tint obj
        /*textTopTintBitmap.applyCanvas (textTopTintCanvas) {
            it.eraseColor(Color.TRANSPARENT)
            save()
            translate(it.width / 2f - layout.getLineMax(0) / 2f, it.height / 2f - layout.height / 2f)
            textPaint.color = textTopTint
            layout.draw(this)
            translate(0f, context.convertDpToPx(1f))
            textPaint.xfermode = dstOutMode
            layout.draw(this)
            textPaint.xfermode = null
            restore()
        }
        canvas.drawBitmap(textTopTintBitmap, 0f, 0f, null)*/

        //draw bottom tint obj
        /*textBottomTintBitmap.applyCanvas (textBottomTintCanvas) {
            it.eraseColor(Color.TRANSPARENT)
            save()
            translate(it.width / 2f - layout.getLineMax(0) / 2f, it.height / 2f - layout.height / 2f)
            textPaint.color = textBottomTint
            layout.draw(this)
            translate(0f, -context.convertDpToPx(1f))
            textPaint.xfermode = dstOutMode
            layout.draw(this)
            textPaint.xfermode = null
            restore()
        }
        canvas.drawBitmap(textBottomTintBitmap, 0f, 0f, null)*/

        //draw light source
        /*path.rewind()
        path.addRoundRect(0, 0, nWidth, nHeight, cornerRadius, cornerRadius, Path.Direction.CCW)
        shaderPaint.shader = radialShader
        canvas.drawPath(path, shaderPaint)

        canvas.restore()*/
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val minSize = context.getDimensionResource(R.dimen.gate_size)
        val textWidth = layout.getLineMax(0).roundToInt()
        val textHeight = layout.height

        val desiredWidth = (if (textWidth > minSize) textWidth else minSize) + paddingStart + paddingEnd
        val desiredHeight = (if (textHeight > minSize) textHeight else minSize) + paddingTop + paddingBottom

        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)

        val width = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> min(widthSize, desiredWidth)
            MeasureSpec.UNSPECIFIED -> desiredWidth
            else -> throw IllegalStateException()
        }

        val height = when (heightMode) {
            MeasureSpec.EXACTLY -> heightSize
            MeasureSpec.AT_MOST -> min(heightSize, desiredHeight)
            MeasureSpec.UNSPECIFIED -> desiredHeight
            else -> throw IllegalStateException()
        }

        setMeasuredDimension(width, height)
    }

    override fun onSaveInstanceState(): Parcelable = SavedState(super.onSaveInstanceState()!!).apply{
        _text = editable
        _textSize = textSize
        _textColor = textColor
        _textTopTint = textTopTint
        _textBottomTint = textBottomTint
        _gateColor = gateColor
        _gateTopTint = gateTopTint
        _gateBottomTint = gateBottomTint
        _cornerRadius = cornerRadius
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        if (state is SavedState) {
            super.onRestoreInstanceState(state.superState)
            using(state) {
                editable.clear()
                editable.append(_text)
                textSize = _textSize
                textColor = _textColor
                textTopTint = _textTopTint
                textBottomTint = _textBottomTint
                gateColor = _gateColor
                gateTopTint = _gateTopTint
                gateBottomTint = _gateBottomTint
                cornerRadius = _cornerRadius
            }
        } else {
            super.onRestoreInstanceState(state)
        }
    }

    private class SavedState: BaseSavedState {
        constructor(superState: Parcelable): super(superState)
        private constructor(parcel: Parcel): super(parcel) {
            using(parcel) {
                _text = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(this)
                _textSize = readInt()
                _textColor = readInt()
                _textTopTint = readInt()
                _textBottomTint = readInt()
                _gateColor = readInt()
                _gateTopTint = readInt()
                _gateBottomTint = readInt()
                _cornerRadius = readInt()
            }
        }

        var _text: CharSequence = ""
        var _textSize: Int = 0
        var _textColor: Int = 0
        var _textTopTint: Int = 0
        var _textBottomTint: Int = 0
        var _gateColor: Int = 0
        var _gateTopTint: Int = 0
        var _gateBottomTint: Int = 0
        var _cornerRadius: Int = 0

        override fun writeToParcel(out: Parcel, flags: Int) = using (out) {
            super.writeToParcel(this, flags)
            TextUtils.writeToParcel(_text, this, 0)
            writeInt(_textSize)
            writeInt(_textColor)
            writeInt(_textTopTint)
            writeInt(_textBottomTint)
            writeInt(_gateColor)
            writeInt(_gateTopTint)
            writeInt(_gateBottomTint)
            writeInt(_cornerRadius)
        }

        companion object {
            @Suppress("unused")
            @JvmField val CREATOR = parcelableCreator(::SavedState)
        }
    }

    @Suppress("UNUSED_PARAMETER")
    private fun <T> refreshLayoutAndReDraw(property: KProperty<*>, oldValue: T, newValue: T) {
        if (oldValue != newValue) {
            requestLayout()
            invalidate()
        }
    }

    @Suppress("UNUSED_PARAMETER")
    private fun <T> reDraw(property: KProperty<*>, oldValue: T, newValue: T) {
        if (oldValue != newValue) invalidate()
    }

    private operator fun Path.minusAssign(other: Path) {
        op(other, Path.Op.DIFFERENCE)
    }

    private fun Path.addRoundRect(left: Int, top: Int, right: Int, bottom: Int, rx: Int, ry: Int, dir: Path.Direction)
            = addRoundRect(left.toFloat(), top.toFloat(), right.toFloat(), bottom.toFloat(), rx.toFloat(), ry.toFloat(), dir)

    private inline fun Bitmap.applyCanvas(canvas: Canvas = Canvas(this), block: Canvas.(Bitmap) -> Unit) = canvas.block(this)

    fun edit(block: Editable.() -> Unit) = editable.block()
}