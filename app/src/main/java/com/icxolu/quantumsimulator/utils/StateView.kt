package com.icxolu.quantumsimulator.utils

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.os.Parcel
import android.os.Parcelable
import android.text.*
import android.util.AttributeSet
import android.view.View
import com.icxolu.quantumsimulator.R
import kotlin.math.min
import kotlin.math.roundToInt
import kotlin.properties.Delegates
import kotlin.reflect.KProperty


class StateView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
): View(context, attrs, defStyleAttr) {

    private val editable by lazy { Editable.Factory.getInstance().newEditable("") }
    private val layout by lazy { DynamicLayout(editable, textPaint, Int.MAX_VALUE, Layout.Alignment.ALIGN_NORMAL, 1f, 0f, false) }

    var text: String by Delegates.observable("") { property, oldValue, newValue ->
        editable.clear()
        editable.append(newValue)
        refreshLayoutAndReDraw(property, oldValue, newValue)
    }

    var textSize: Int by Delegates.observable(context.convertDpToPx(27), this::refreshLayoutAndReDraw)
    var textColor: Int by Delegates.observable(Color.BLACK, this::reDraw)

    init {
        val ta = context.theme.obtainStyledAttributes(attrs, R.styleable.StateView, defStyleAttr, /*R.style.UsergateDefaultStyle*/0)
        try {
            text = ta.getString(R.styleable.StateView_text) ?: ""
            textSize = ta.getDimensionPixelSize(R.styleable.StateView_textSize, textSize)
            textColor = ta.getColor(R.styleable.StateView_textColor, textColor)
        } finally {
            ta.recycle()
        }
    }

    private val textPaint by lazy { TextPaint(Paint.ANTI_ALIAS_FLAG).apply {
        color = textColor
        textSize = this@StateView.textSize.toFloat()
        textAlign = Paint.Align.LEFT
    } }

    private val linePaint by lazy { Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = textColor
        strokeWidth = context.convertDpToPx(2).toFloat()
        style = Paint.Style.STROKE
    } }

    private val dist = context.convertDpToPx(5).toFloat()
    private val angleDist = context.convertDpToPx(7).toFloat()

    private val path by lazy { Path() }

    private val nWidth: Int get() = width - paddingStart - paddingEnd
    private val nHeight: Int get() = height - paddingTop - paddingBottom

    override fun onDraw(canvas: Canvas) {
        canvas.save()
        canvas.translate(paddingStart.toFloat(), paddingTop.toFloat())

        //centering
        canvas.save()
        canvas.translate(nWidth / 2f - layout.getLineMax(0) / 2f - angleDist / 2f, nHeight / 2f - layout.height / 2f)

        //text
        layout.draw(canvas)

        //line before
        path.rewind()
        path.moveTo(-dist, 0f)
        path.lineTo(-dist, layout.height.toFloat())

        //angle after
        path.moveTo(layout.getLineMax(0) + dist, 0f)
        path.rLineTo(angleDist, layout.height / 2f)
        path.rLineTo(-angleDist, layout.height / 2f)
        canvas.drawPath(path, linePaint)

        canvas.restore()

        canvas.restore()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val minSizeHeight = context.getDimensionResource(R.dimen.gate_size)
        val textWidth = (layout.getLineMax(0) + 2 * dist + angleDist + linePaint.strokeWidth).roundToInt()
        val textHeight = layout.height

        val desiredWidth = textWidth + paddingStart + paddingEnd
        val desiredHeight = (if (textHeight > minSizeHeight) textHeight else minSizeHeight) + paddingTop + paddingBottom

        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)

        val width = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> min(widthSize, desiredWidth)
            MeasureSpec.UNSPECIFIED -> desiredWidth
            else -> throw IllegalStateException()
        }

        val height = when (heightMode) {
            MeasureSpec.EXACTLY -> heightSize
            MeasureSpec.AT_MOST -> min(heightSize, desiredHeight)
            MeasureSpec.UNSPECIFIED -> desiredHeight
            else -> throw IllegalStateException()
        }

        setMeasuredDimension(width, height)
    }

    override fun onSaveInstanceState(): Parcelable = SavedState(super.onSaveInstanceState()!!).apply{
        _text = editable
        _textSize = textSize
        _textColor = textColor
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        if (state is SavedState) {
            super.onRestoreInstanceState(state.superState)
            using(state) {
                editable.clear()
                editable.append(_text)
                textSize = _textSize
                textColor = _textColor
            }
        } else {
            super.onRestoreInstanceState(state)
        }
    }

    private class SavedState: BaseSavedState {
        constructor(superState: Parcelable): super(superState)
        private constructor(parcel: Parcel): super(parcel) {
            using(parcel) {
                _text = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel)
                _textSize = readInt()
                _textColor = readInt()
            }
        }

        var _text: CharSequence = ""
        var _textSize: Int = 0
        var _textColor: Int = 0

        override fun writeToParcel(out: Parcel, flags: Int) = using (out) {
            super.writeToParcel(this, flags)
            TextUtils.writeToParcel(_text, this, 0)
            writeInt(_textSize)
            writeInt(_textColor)
        }

        companion object {
            @Suppress("unused")
            @JvmField val CREATOR = parcelableCreator(::SavedState)
        }
    }

    @Suppress("UNUSED_PARAMETER")
    private fun <T> refreshLayoutAndReDraw(property: KProperty<*>, oldValue: T, newValue: T) {
        if (oldValue != newValue) {
            requestLayout()
            invalidate()
        }
    }

    @Suppress("UNUSED_PARAMETER")
    private fun <T> reDraw(property: KProperty<*>, oldValue: T, newValue: T) {
        if (oldValue != newValue) invalidate()
    }

    fun edit(block: Editable.() -> Unit) = editable.block()
}