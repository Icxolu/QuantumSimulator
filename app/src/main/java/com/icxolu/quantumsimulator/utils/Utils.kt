package com.icxolu.quantumsimulator.utils

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.TypedValue
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import android.os.Parcelable
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Parcel
import android.provider.MediaStore
import androidx.annotation.DimenRes
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat


fun Context.convertDpToPx(dp: Float): Float =
        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics)
fun Context.convertDpToPx(dp: Int): Int = convertDpToPx(dp.toFloat()).toInt()
fun Context.convertPxToSp(px: Float): Float = px / resources.displayMetrics.scaledDensity

fun Context.getDimensionResource(@DimenRes id: Int) = resources.getDimension(id).toInt()

@ColorInt
fun Context.getColorFromAttr(@AttrRes attrColor: Int): Int {
    val resolvedAttr = TypedValue()
    theme.resolveAttribute(attrColor, resolvedAttr, true)
    val colorRes = resolvedAttr.run { if (resourceId != 0) resourceId else data }
    return ContextCompat.getColor(this, colorRes)
}

fun requestPermission(activity: Activity, permission: String, requestCode: Int) {
    ActivityCompat.requestPermissions(activity, Array(1) { permission }, requestCode)
}

fun Canvas.drawLine(startX: Int, startY: Int, stopX: Int, stopY: Int, paint: Paint) =
        drawLine(startX.toFloat(), startY.toFloat(), stopX.toFloat(), stopY.toFloat(), paint)

fun deciToBin(n: Int, len: Int): ArrayList<Int> {
    var quotient: Int = n
    val binaryNumber: ArrayList<Int> = arrayListOf()
    var remainder: Int

    while (quotient != 0) {
        remainder = quotient % 2
        quotient /= 2
        binaryNumber.add(0, remainder)
    }
    while (len > binaryNumber.size) binaryNumber.add(0, 0)

    return binaryNumber
}

fun deciToBinStr(n: Int, len: Int): String = deciToBin(n, len).joinToString(separator = "")

fun binToDeci(array: ArrayList<Int>): Int {
    var binaryNumber = array.joinToString(separator = "").toLong()
    var decimalNumber = 0
    var i = 0
    var remainder: Long

    while (binaryNumber.toInt() != 0) {
        remainder = binaryNumber % 10
        binaryNumber /= 10
        decimalNumber += (remainder * Math.pow(2.0, i.toDouble())).toInt()
        ++i
    }
    return decimalNumber
}

fun binToDeciStr(array: Long): Int {
    var binaryNumber = array
    var decimalNumber = 0
    var i = 0
    var remainder: Long

    while (binaryNumber.toInt() != 0) {
        remainder = binaryNumber % 10
        binaryNumber /= 10
        decimalNumber += (remainder * Math.pow(2.0, i.toDouble())).toInt()
        ++i
    }
    return decimalNumber
}

inline fun <T> using(receiver: T, block: T.() -> Unit) = receiver.block()
inline fun <T, R> using(receiver: T, parameter: R, block: T.(R) -> Unit) = receiver.block(parameter)

/**
 * @throws ClassCastException if not all items in the List are from type T
 */
@Suppress("UNCHECKED_CAST")
inline fun <reified T> List<*>.checkItemsAre(message: String = "Some items can't be cast to ${T::class.java.simpleName}") = this as? List<T> ?: throw ClassCastException(message)

@JvmName("pairIterableIterator")
operator fun <A, B> Pair<Iterable<A>, Iterable<B>>.iterator(): Iterator<Pair<A, B>> = object: Iterator<Pair<A, B>> {
    val i1 = first.iterator()
    val i2 = second.iterator()
    override fun hasNext(): Boolean = i1.hasNext() && i2.hasNext()
    override fun next(): Pair<A, B> = Pair(i1.next(), i2.next())
}

@JvmName("pairArrayIterator")
operator fun <A, B> Pair<Array<A>, Array<B>>.iterator(): Iterator<Pair<A, B>> = object: Iterator<Pair<A, B>> {
    val i1 = first.iterator()
    val i2 = second.iterator()
    override fun hasNext(): Boolean = i1.hasNext() && i2.hasNext()
    override fun next(): Pair<A, B> = Pair(i1.next(), i2.next())
}

@Suppress("unused")
@JvmName("pairIterableForEach")
inline fun <A, B>  Pair<Iterable<A>, Iterable<B>>.forEach(action: (A, B) -> Unit) {
    for ((a, b) in this) action(a, b)
}

@JvmName("pairArrayForEach")
inline fun <A, B>  Pair<Array<A>, Array<B>>.forEach(action: (A, B) -> Unit) {
    for ((a, b) in this) action(a, b)
}

@Suppress("unused")
@JvmName("pairIterableForEachIndexed")
inline fun <A, B> Pair<Iterable<A>, Iterable<B>>.forEachIndexed(action: (Int, A, B) -> Unit) {
    var i = 0
    for ((a, b) in this) action(i++, a, b)
}

@JvmName("pairArrayForEachIndexed")
inline fun <A, B> Pair<Array<A>, Array<B>>.forEachIndexed(action: (Int, A, B) -> Unit) {
    var i = 0
    for ((a, b) in this) action(i++, a, b)
}

inline fun <reified T : Parcelable> Intent.getParcelableExtraSDK(key: String): T? = when {
    Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU -> @Suppress("DEPRECATION") getParcelableExtra(key)
    else -> getParcelableExtra(key, T::class.java)
}

inline fun <reified T : Parcelable> Bundle.getParcelableSDK(key: String): T? = when {
    Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU -> @Suppress("DEPRECATION") getParcelable(key)
    else -> getParcelable(key, T::class.java)
}

inline fun <reified T : Parcelable> Parcel.readArrayListSDK(): ArrayList<T>? = when {
    Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU -> @Suppress("DEPRECATION") readArrayList(T::class.java.classLoader) as? ArrayList<T>?
    else -> readArrayList(T::class.java.classLoader, T::class.java)
}

fun Context.saveImage(bitmap: Bitmap): Uri? {
    // https://stackoverflow.com/a/66817176
    val filename = "IMG_${System.currentTimeMillis()}.png"
    val contentValues = ContentValues().apply {
        put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
        put(MediaStore.MediaColumns.MIME_TYPE, "image/png")
        put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
        put(MediaStore.Video.Media.IS_PENDING, 1)
    }

    val resolver = applicationContext.contentResolver
    val imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
    val fos = imageUri?.let { resolver.openOutputStream(it) }

    fos?.use { bitmap.compress(Bitmap.CompressFormat.PNG, 100, it) }
    contentValues.clear()
    contentValues.put(MediaStore.Video.Media.IS_PENDING, 0)
    imageUri?.let { resolver.update(it, contentValues, null, null) }

    return imageUri
}