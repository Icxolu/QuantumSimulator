package com.icxolu.quantumsimulator.utils

import android.os.Parcel
import ch.obermuhlner.math.big.BigDecimalMath
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode
import kotlin.math.pow

data class Complex(@Suppress("MemberVisibilityCanBePrivate") val real: BigDecimal, @Suppress("MemberVisibilityCanBePrivate") val imaginary: BigDecimal): KParcelable {
    constructor(real: Number, imaginary: Number): this(BigDecimal("$real"), BigDecimal("$imaginary"))
    constructor(real: String, imaginary: String): this(BigDecimal(real), BigDecimal(imaginary))
    constructor(source: Parcel): this(
            source.readSerializable() as BigDecimal,
            source.readSerializable() as BigDecimal
    )

    companion object {
        @JvmField val CREATOR = parcelableCreator(::Complex)
        val ZERO = Complex(BigDecimal.ZERO, BigDecimal.ZERO)
        val ONE = Complex(BigDecimal.ONE, BigDecimal.ZERO)
        val i = Complex(BigDecimal.ZERO, BigDecimal.ONE)

        val MATH_CTX = MathContext(100, RoundingMode.HALF_UP)

        fun createFromAngle(phase: BigDecimal, radius: BigDecimal = BigDecimal.ONE) = Complex(radius * cos(phase), radius * sin(phase))
    }

    fun toStringMath(): String {
        val realStr = real.abs().setScale(3, BigDecimal.ROUND_HALF_UP).toString().trimEnd('.', '0')
        val imaginaryStr = imaginary.abs().setScale(3, BigDecimal.ROUND_HALF_UP).toString().trimEnd('.', '0')
        val precision = BigDecimal(1E-4)

        val realSgn = if (real.signum() < 0) " - " else " + "
        val imaginarySgn = if (imaginary.signum() < 0) " - " else " + "
        var str: String

        str = when {
            real.abs() < precision -> imaginarySgn + imaginaryStr + "i"
            imaginary.abs() < precision -> realSgn + realStr
            else -> " + (" + if (realSgn == " - ") { realSgn } else { "" } + realStr + imaginarySgn + imaginaryStr + "i)"
        }

        if (str != "") {
            str += " \u00b7 "
        }

        return str
    }

    fun abs(): Double {
        return Math.hypot(real.toDouble(), imaginary.toDouble())
    }

    fun absSquare(): Double {
        return real.toDouble().pow(2)+imaginary.toDouble().pow(2)
    }

    fun reciprocal(): Complex {
        val scale = real * real + imaginary * imaginary
        return Complex(real / scale, -imaginary / scale)
    }

    operator fun unaryMinus() = Complex(-real, -imaginary)
    operator fun unaryPlus() = this

    operator fun plus(other: BigDecimal) = Complex(real + other, imaginary)
    operator fun plus(other: Complex) = Complex(real + other.real, imaginary + other.imaginary)

    operator fun minus(other: BigDecimal) = Complex(real - other, imaginary)
    operator fun minus(other: Complex) = Complex(real - other.real, imaginary - other.imaginary)

    operator fun times(other: BigDecimal) = Complex(real * other, imaginary * other)
    operator fun times(other: Complex) = Complex(
            real * other.real - imaginary * other.imaginary,
            real * other.imaginary + imaginary * other.real
    )
    operator fun times(other: Array<Array<Complex>>): Array<Array<Complex>> {
        val result = other.copy()
        result.forEach { list ->
            list.forEachIndexed { i, complex ->
                list[i] = this * complex
            }
        }
        return result
    }

    operator fun div(other: BigDecimal) = Complex(real / other, imaginary / other)
    operator fun div(other: Complex) = this * other.reciprocal()

    override fun equals(other: Any?): Boolean {
        return if (other !is Complex) super.equals(other)
        else {
            real == other.real && imaginary == other.imaginary
        }
    }

    override fun hashCode(): Int {
        var result = real.hashCode()
        result = 31 * result + imaginary.hashCode()
        return result
    }

    override fun toString(): String {
        return "Complex(real=${real.setScale(2, RoundingMode.HALF_UP)}, imaginary=${imaginary.setScale(2, RoundingMode.HALF_UP)})"
    }

    override fun writeToParcel(dest: Parcel, flags: Int) = using(dest) {
        writeSerializable(real)
        writeSerializable(imaginary)
    }
}

operator fun BigDecimal.plus(other: Complex) = other + this
operator fun BigDecimal.plus(other: BigDecimal): BigDecimal = this.add(other, Complex.MATH_CTX)

operator fun BigDecimal.minus(other: Complex) = -other + this
operator fun BigDecimal.minus(other: BigDecimal): BigDecimal = this.subtract(other, Complex.MATH_CTX)

operator fun BigDecimal.times(other: Complex) = other * this
operator fun BigDecimal.times(other: BigDecimal): BigDecimal = this.multiply(other, Complex.MATH_CTX)

operator fun BigDecimal.div(other: Complex) = this * other.reciprocal()
operator fun BigDecimal.div(other: BigDecimal): BigDecimal = this.divide(other, Complex.MATH_CTX)
operator fun BigDecimal.div(other: Int): BigDecimal = this / BigDecimal(other)

operator fun BigDecimal.times(other: Array<Array<Complex>>): Array<Array<Complex>> {
    val result = other.copy()
    result.forEach { list ->
        list.forEachIndexed { j, complex ->
            list[j] = complex * this
        }
    }
    return result
}

operator fun Array<Array<Complex>>.times(other: BigDecimal): Array<Array<Complex>> {
    val result = this.copy()
    result.forEach { list ->
        list.forEachIndexed { j, complex ->
            list[j] = complex * other
        }
    }
    return result
}

operator fun Array<Array<Complex>>.div(other: BigDecimal): Array<Array<Complex>> {
    val result = this.copy()
    result.forEach { list->
        list.forEachIndexed { j, complex ->
            list[j] = complex / other
        }
    }
    return result
}

operator fun Array<Array<Complex>>.plus(other: Array<Array<Complex>>): Array<Array<Complex>> {
    val result = this.copy()
    Pair(result, other).forEach { first, second ->
        Pair(first, second).forEachIndexed { i, c1, c2 ->
            first[i] = c1 + c2
        }
    }
    return result
}

operator fun Array<Array<Complex>>.minus(other: Array<Array<Complex>>): Array<Array<Complex>> {
    val result = this.copy()
    Pair(result, other).forEach { first, second ->
        Pair(first, second).forEachIndexed { i, c1, c2 ->
            first[i] = c1 - c2
        }
    }
    return result
}

operator fun Array<Complex>.times(other: BigDecimal): Array<Complex> {
    val result = this.clone()
    result.forEachIndexed { i, complex ->
        result[i] = complex * other
    }
    return result
}

operator fun Array<Complex>.div(other: BigDecimal): Array<Complex> {
    val result = this.clone()
    result.forEachIndexed { i, complex ->
        result[i] = complex / other
    }
    return result
}

fun Array<Array<Complex>>.copy() = Array(size) { get(it).clone() }

fun sqrt(x: BigDecimal): BigDecimal = BigDecimalMath.sqrt(x, Complex.MATH_CTX)
fun cos(x: BigDecimal): BigDecimal = BigDecimalMath.cos(x, Complex.MATH_CTX)
fun sin(x: BigDecimal): BigDecimal = BigDecimalMath.sin(x, Complex.MATH_CTX)
fun BigDecimal(value: CharSequence) = BigDecimal(value.toString())