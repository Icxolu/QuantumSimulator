package com.icxolu.quantumsimulator.utils

import android.app.Application
import java.util.*

class Globals: Application() {
    val rowsWithGates: SortedSet<Int> = TreeSet()
}