package com.icxolu.quantumsimulator.utils

import android.os.Bundle
import android.app.Dialog
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

class CreateManyQubitsDialog : DialogFragment() {
    companion object {
        fun newInstance() = CreateManyQubitsDialog()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        return activity?.let {

            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)
            builder.setMessage("Only up to 5 qubits are supported by now").setNegativeButton("Ok", null)

            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}