package com.icxolu.quantumsimulator.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import java.io.Serializable
import kotlin.jvm.internal.CallableReference
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KClass
import kotlin.reflect.KDeclarationContainer
import kotlin.reflect.KProperty

@Suppress("UNCHECKED_CAST")
inline fun <reified T> intentExtra(customName: String? = null): BaseExtra<Intent, T> = when (T::class) {
    Byte::class -> IntentByteExtra(customName)
    Short::class -> IntentShortExtra(customName)
    Int::class -> IntentIntExtra(customName)
    Long::class -> IntentLongExtra(customName)
    Float::class -> IntentFloatExtra(customName)
    Double::class -> IntentDoubleExtra(customName)
    Boolean::class -> IntentBooleanExtra(customName)
    Char::class -> IntentCharExtra(customName)
    CharSequence::class -> IntentCharSequenceExtra(customName)
    String::class -> IntentStringExtra(customName)
    Bundle::class -> IntentBundleExtra(customName)
    Serializable::class -> IntentSerializableExtra(customName)
    Parcelable::class -> IntentParcelableExtra(customName)
    else -> throw UnsupportedOperationException("This type is yet not supported")
} as BaseExtra<Intent, T>

@Suppress("UNCHECKED_CAST")
inline fun <reified T> bundleExtra(customName: String? = null): BaseExtra<Bundle, T> = when(T::class) {
    Byte::class -> BundleByteExtra(customName)
    Short::class -> BundleShortExtra(customName)
    Int::class -> BundleIntExtra(customName)
    Long::class -> BundleLongExtra(customName)
    Float::class -> BundleFloatExtra(customName)
    Double::class -> BundleDoubleExtra(customName)
    Boolean::class -> BundleBooleanExtra(customName)
    Char::class -> BundleCharExtra(customName)
    CharSequence::class -> BundleCharSequenceExtra(customName)
    String::class -> BundleStringExtra(customName)
    Bundle::class -> BundleBundleExtra(customName)
    Serializable::class -> BundleSerializableExtra(customName)
    Parcelable::class -> BundleParcelableExtra(customName)
    else -> throw UnsupportedOperationException("This type is yet not supported")
} as BaseExtra<Bundle, T>

abstract class BaseExtra<in R, T>(private val customName: String? = null): ReadWriteProperty<R, T> {
    protected val KProperty<*>.extraName get() = customName ?: fallbackName
    private val KProperty<*>.fallbackName get() = ownerCanonicalName?.let { "$it::$name" } ?: name
}
class IntentByteExtra(customName: String? = null): BaseExtra<Intent, Byte>(customName) {
    override fun getValue(thisRef: Intent, property: KProperty<*>): Byte
            = thisRef.getByteExtra(property.extraName, -1)

    override fun setValue(thisRef: Intent, property: KProperty<*>, value: Byte) {
        thisRef.putExtra(property.extraName, value)
    }
}
class IntentShortExtra(customName: String? = null): BaseExtra<Intent, Short>(customName) {
    override fun getValue(thisRef: Intent, property: KProperty<*>): Short
            = thisRef.getShortExtra(property.extraName, -1)

    override fun setValue(thisRef: Intent, property: KProperty<*>, value: Short) {
        thisRef.putExtra(property.extraName, value)
    }
}
class IntentIntExtra(customName: String? = null): BaseExtra<Intent, Int>(customName) {

    override fun getValue(thisRef: Intent, property: KProperty<*>): Int
            = thisRef.getIntExtra(property.extraName, -1)
    override fun setValue(thisRef: Intent, property: KProperty<*>, value: Int) {
        thisRef.putExtra(property.extraName, value)
    }
}
class IntentLongExtra(customName: String? = null): BaseExtra<Intent, Long>(customName) {
    override fun getValue(thisRef: Intent, property: KProperty<*>): Long
            = thisRef.getLongExtra(property.extraName, -1L)

    override fun setValue(thisRef: Intent, property: KProperty<*>, value: Long) {
        thisRef.putExtra(property.extraName, value)
    }
}
class IntentFloatExtra(customName: String? = null): BaseExtra<Intent, Float>(customName) {
    override fun getValue(thisRef: Intent, property: KProperty<*>): Float
            = thisRef.getFloatExtra(property.extraName, -1f)

    override fun setValue(thisRef: Intent, property: KProperty<*>, value: Float) {
        thisRef.putExtra(property.extraName, value)
    }
}
class IntentDoubleExtra(customName: String? = null): BaseExtra<Intent, Double>(customName) {
    override fun getValue(thisRef: Intent, property: KProperty<*>): Double
            = thisRef.getDoubleExtra(property.extraName, -1.0)

    override fun setValue(thisRef: Intent, property: KProperty<*>, value: Double) {
        thisRef.putExtra(property.extraName, value)
    }
}
class IntentBooleanExtra(customName: String? = null): BaseExtra<Intent, Boolean>(customName) {

    override fun getValue(thisRef: Intent, property: KProperty<*>): Boolean
            = thisRef.getBooleanExtra(property.extraName, false)
    override fun setValue(thisRef: Intent, property: KProperty<*>, value: Boolean) {
        thisRef.putExtra(property.extraName, value)
    }
}
class IntentCharExtra(customName: String? = null): BaseExtra<Intent, Char>(customName) {
    override fun getValue(thisRef: Intent, property: KProperty<*>): Char
            = thisRef.getCharExtra(property.extraName, '\u0000')

    override fun setValue(thisRef: Intent, property: KProperty<*>, value: Char) {
        thisRef.putExtra(property.extraName, value)
    }
}
class IntentCharSequenceExtra(customName: String? = null): BaseExtra<Intent, CharSequence>(customName) {

    override fun getValue(thisRef: Intent, property: KProperty<*>): CharSequence
            = thisRef.getCharSequenceExtra(property.extraName) ?: ""
    override fun setValue(thisRef: Intent, property: KProperty<*>, value: CharSequence) {
        thisRef.putExtra(property.extraName, value)
    }
}
class IntentStringExtra(customName: String? = null): BaseExtra<Intent, String>(customName) {

    override fun getValue(thisRef: Intent, property: KProperty<*>): String
            = thisRef.getStringExtra(property.extraName) ?: ""
    override fun setValue(thisRef: Intent, property: KProperty<*>, value: String) {
        thisRef.putExtra(property.extraName, value)
    }
}
class IntentBundleExtra(customName: String? = null): BaseExtra<Intent, Bundle?>(customName) {
    override fun getValue(thisRef: Intent, property: KProperty<*>): Bundle?
            = thisRef.getBundleExtra(property.extraName)

    override fun setValue(thisRef: Intent, property: KProperty<*>, value: Bundle?) {
        thisRef.putExtra(property.extraName, value)
    }
}
class IntentSerializableExtra(customName: String? = null): BaseExtra<Intent, Serializable?>(customName) {
    override fun getValue(thisRef: Intent, property: KProperty<*>): Serializable?
            = thisRef.getSerializableExtra(property.extraName)

    override fun setValue(thisRef: Intent, property: KProperty<*>, value: Serializable?) {
        thisRef.putExtra(property.extraName, value)
    }
}
class IntentParcelableExtra(customName: String? = null): BaseExtra<Intent, Parcelable>(customName) {
    override fun getValue(thisRef: Intent, property: KProperty<*>): Parcelable
            = thisRef.getParcelableExtra(property.extraName)
            ?: throw NullPointerException("The requested parcelable extra doesn't exist. You may not have set ${property.extraName}")

    override fun setValue(thisRef: Intent, property: KProperty<*>, value: Parcelable) {
        thisRef.putExtra(property.extraName, value)
    }
}

class BundleByteExtra(customName: String? = null): BaseExtra<Bundle, Byte>(customName) {
    override fun getValue(thisRef: Bundle, property: KProperty<*>): Byte = thisRef.getByte(property.extraName, -1)

    override fun setValue(thisRef: Bundle, property: KProperty<*>, value: Byte) {
        thisRef.putByte(property.extraName, value)
    }
}
class BundleShortExtra(customName: String? = null): BaseExtra<Bundle, Short>(customName) {
    override fun getValue(thisRef: Bundle, property: KProperty<*>): Short = thisRef.getShort(property.extraName, -1)

    override fun setValue(thisRef: Bundle, property: KProperty<*>, value: Short) {
        thisRef.putShort(property.extraName, value)
    }
}
class BundleIntExtra(customName: String? = null): BaseExtra<Bundle, Int>(customName) {
    override fun getValue(thisRef: Bundle, property: KProperty<*>): Int = thisRef.getInt(property.extraName, -1)

    override fun setValue(thisRef: Bundle, property: KProperty<*>, value: Int) {
        thisRef.putInt(property.extraName, value)
    }
}
class BundleLongExtra(customName: String? = null): BaseExtra<Bundle, Long>(customName) {
    override fun getValue(thisRef: Bundle, property: KProperty<*>): Long = thisRef.getLong(property.extraName, -1L)

    override fun setValue(thisRef: Bundle, property: KProperty<*>, value: Long) {
        thisRef.putLong(property.extraName, value)
    }
}
class BundleFloatExtra(customName: String? = null): BaseExtra<Bundle, Float>(customName) {
    override fun getValue(thisRef: Bundle, property: KProperty<*>): Float = thisRef.getFloat(property.extraName, -1f)

    override fun setValue(thisRef: Bundle, property: KProperty<*>, value: Float) {
        thisRef.putFloat(property.extraName, value)
    }
}
class BundleDoubleExtra(customName: String? = null): BaseExtra<Bundle, Double>(customName) {
    override fun getValue(thisRef: Bundle, property: KProperty<*>): Double = thisRef.getDouble(property.extraName, -1.0)

    override fun setValue(thisRef: Bundle, property: KProperty<*>, value: Double) {
        thisRef.putDouble(property.extraName, value)
    }
}
class BundleBooleanExtra(customName: String? = null): BaseExtra<Bundle, Boolean>(customName) {
    override fun getValue(thisRef: Bundle, property: KProperty<*>): Boolean = thisRef.getBoolean(property.extraName, false)

    override fun setValue(thisRef: Bundle, property: KProperty<*>, value: Boolean) {
        thisRef.putBoolean(property.extraName, value)
    }
}
class BundleCharExtra(customName: String? = null): BaseExtra<Bundle, Char>(customName) {
    override fun getValue(thisRef: Bundle, property: KProperty<*>): Char = thisRef.getChar(property.extraName, '\u0000')

    override fun setValue(thisRef: Bundle, property: KProperty<*>, value: Char) {
        thisRef.putChar(property.extraName, value)
    }
}
class BundleCharSequenceExtra(customName: String? = null): BaseExtra<Bundle, CharSequence>(customName) {
    override fun getValue(thisRef: Bundle, property: KProperty<*>): CharSequence = thisRef.getCharSequence(property.extraName, "")

    override fun setValue(thisRef: Bundle, property: KProperty<*>, value: CharSequence) {
        thisRef.putCharSequence(property.extraName, value)
    }
}
class BundleStringExtra(customName: String? = null): BaseExtra<Bundle, String>(customName) {
    override fun getValue(thisRef: Bundle, property: KProperty<*>): String = thisRef.getString(property.extraName, "")

    override fun setValue(thisRef: Bundle, property: KProperty<*>, value: String) {
        thisRef.putString(property.extraName, value)
    }
}
class BundleBundleExtra(customName: String? = null): BaseExtra<Bundle, Bundle>(customName) {
    override fun getValue(thisRef: Bundle, property: KProperty<*>): Bundle = thisRef.getBundle(property.extraName)!!

    override fun setValue(thisRef: Bundle, property: KProperty<*>, value: Bundle) {
        thisRef.putBundle(property.extraName, value)
    }
}
class BundleSerializableExtra(customName: String? = null): BaseExtra<Bundle, Serializable>(customName) {
    override fun getValue(thisRef: Bundle, property: KProperty<*>): Serializable = thisRef.getSerializable(property.extraName)!!

    override fun setValue(thisRef: Bundle, property: KProperty<*>, value: Serializable) {
        thisRef.putSerializable(property.extraName, value)
    }
}
class BundleParcelableExtra(customName: String? = null): BaseExtra<Bundle, Parcelable>(customName) {
    override fun getValue(thisRef: Bundle, property: KProperty<*>): Parcelable = thisRef.getParcelable(property.extraName)!!

    override fun setValue(thisRef: Bundle, property: KProperty<*>, value: Parcelable) {
        thisRef.putParcelable(property.extraName, value)
    }
}


private val KProperty<*>.owner: KDeclarationContainer? get() = (this as? CallableReference)?.owner
private val KDeclarationContainer.canonicalName: String? get() = (this as? KClass<*>)?.java?.canonicalName
private val KProperty<*>.ownerCanonicalName: String? get() = owner?.canonicalName

abstract class ActivityCompanion(val kClass: KClass<out Activity>) {
    @Suppress("MemberVisibilityCanBePrivate") fun intent(context: Context) = Intent(context, kClass.java)
    inline fun intent(context: Context, configure: Intent.() -> Unit) = Intent(context, kClass.java).apply(configure)
    fun start(context: Context) { context.startActivity(intent(context)) }
    inline fun start(context: Context, configure: Intent.() -> Unit) { context.startActivity(intent(context, configure)) }
    fun startForResult(activity: Activity, requestCode: Int) { activity.startActivityForResult(intent(activity), requestCode) }
    @Suppress("unused") inline fun startForResult(activity: Activity, requestCode: Int, configure: Intent.() -> Unit) { activity.startActivityForResult(intent(activity, configure), requestCode) }
}