package com.icxolu.quantumsimulator.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import kotlin.reflect.KProperty


object PrefsHelper {
    fun defaultPrefs(context: Context): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    @Suppress("unused")
    fun customPrefs(context: Context, name: String, mode: Int = Context.MODE_PRIVATE): SharedPreferences
            = context.getSharedPreferences(name, mode)
}

inline fun SharedPreferences.edit(operation: SharedPreferences.Editor.() -> Unit) {
    val editor = edit()
    operation(editor)
    editor.apply()
}

operator fun SharedPreferences.set(key: String, value: Any) {
    when (value) {
        is String -> edit { putString(key, value) }
        is Int -> edit { putInt(key, value) }
        is Long -> edit { putLong(key, value) }
        is Float -> edit { putFloat(key, value) }
        is Boolean -> edit { putBoolean(key, value) }
        else -> throw UnsupportedOperationException("Type not yet supported")
    }
}

@Suppress("IMPLICIT_CAST_TO_ANY")
inline operator fun <reified T: Any> SharedPreferences.get(key: String, defaultValue: T? = null): T = when (T::class) {
    String::class -> getString(key, defaultValue as? String ?: "")
    Int::class -> getInt(key, defaultValue as? Int ?: -1)
    Long::class -> getLong(key, defaultValue as? Long ?: -1L)
    Float::class -> getFloat(key, defaultValue as? Float ?: -1f)
    Boolean::class -> getBoolean(key, defaultValue as? Boolean ?: false)
    else -> throw UnsupportedOperationException("Type not yet supported")
} as T

@Suppress("UNCHECKED_CAST")
inline fun <reified T> sharedPrefs(customName: String? = null, noinline defaultValue: () -> T? = { null }): BaseExtra<SharedPreferences, T> {
    val lazy = lazy(defaultValue)
    return when (T::class) {
        String::class -> SharedPreferencesStringExtra(customName, lazy as Lazy<String?>)
        Int::class -> SharedPreferencesIntExtra(customName, lazy as Lazy<Int?>)
        Long::class -> SharedPreferencesLongExtra(customName, lazy as Lazy<Long?>)
        Float::class -> SharedPreferencesFloatExtra(customName, lazy as Lazy<Float?>)
        Boolean::class -> SharedPreferencesBooleanExtra(customName, lazy as Lazy<Boolean?>)
        else -> throw UnsupportedOperationException("Type not yet supported")
    } as BaseExtra<SharedPreferences, T>
}

class SharedPreferencesStringExtra(customName: String? = null, private val defaultValue: Lazy<String?>): BaseExtra<SharedPreferences, String>(customName) {
    override fun getValue(thisRef: SharedPreferences, property: KProperty<*>): String = thisRef[property.extraName, defaultValue.value]
    override fun setValue(thisRef: SharedPreferences, property: KProperty<*>, value: String) {
        thisRef[property.extraName] = value
    }
}
class SharedPreferencesIntExtra(customName: String? = null, private val defaultValue: Lazy<Int?>): BaseExtra<SharedPreferences, Int>(customName) {
    override fun getValue(thisRef: SharedPreferences, property: KProperty<*>): Int = thisRef[property.extraName, defaultValue.value]
    override fun setValue(thisRef: SharedPreferences, property: KProperty<*>, value: Int) {
        thisRef[property.extraName] = value
    }
}
class SharedPreferencesLongExtra(customName: String? = null, private val defaultValue: Lazy<Long?>): BaseExtra<SharedPreferences, Long>(customName) {
    override fun getValue(thisRef: SharedPreferences, property: KProperty<*>): Long = thisRef[property.extraName, defaultValue.value]
    override fun setValue(thisRef: SharedPreferences, property: KProperty<*>, value: Long) {
        thisRef[property.extraName] = value
    }
}
class SharedPreferencesFloatExtra(customName: String? = null, private val defaultValue: Lazy<Float?>): BaseExtra<SharedPreferences, Float>(customName) {
    override fun getValue(thisRef: SharedPreferences, property: KProperty<*>): Float = thisRef[property.extraName, defaultValue.value]
    override fun setValue(thisRef: SharedPreferences, property: KProperty<*>, value: Float) {
        thisRef[property.extraName] = value
    }
}
class SharedPreferencesBooleanExtra(customName: String? = null, private val defaultValue: Lazy<Boolean?>): BaseExtra<SharedPreferences, Boolean>(customName) {
    override fun getValue(thisRef: SharedPreferences, property: KProperty<*>): Boolean = thisRef[property.extraName, defaultValue.value]
    override fun setValue(thisRef: SharedPreferences, property: KProperty<*>, value: Boolean) {
        thisRef[property.extraName] = value
    }
}