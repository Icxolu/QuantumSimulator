package com.icxolu.quantumsimulator.utils

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.util.AttributeSet
import android.util.TypedValue
import android.view.DragEvent

class MyRV @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {
    private val scrollDelay = 250L
    private val scrollInterval = 100L
    private val scrollSpeed = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20f, resources.displayMetrics).toInt()
    private val scrollMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25f, resources.displayMetrics).toInt()

    private var doScrollRight = false
    private var doScrollLeft = false
    private var scrollActive = false

    private val scrollHandler = object: Runnable {
        override fun run() {
            if (doScrollRight && canScrollHorizontally(1)) {
                scrollActive = true
                smoothScrollBy(scrollSpeed, 0)
                postDelayed(this, scrollInterval)
            } else if (doScrollLeft && canScrollHorizontally(-1)) {
                scrollActive = true
                smoothScrollBy(-scrollSpeed, 0)
                postDelayed(this, scrollInterval)
            } else {
                scrollActive = false
            }
        }
    }

    override fun dispatchDragEvent(event: DragEvent): Boolean {
        when (event.action) {
            DragEvent.ACTION_DRAG_ENTERED -> doScrollRight = true
            DragEvent.ACTION_DRAG_EXITED, DragEvent.ACTION_DRAG_ENDED, DragEvent.ACTION_DROP -> doScrollRight = false
            DragEvent.ACTION_DRAG_LOCATION -> handleLocation(event.x)
        }
        return super.dispatchDragEvent(event)
    }

    private fun handleLocation(x: Float) {
        doScrollRight = x >= width - scrollMargin
        doScrollLeft = x <= scrollMargin

        if ((doScrollRight || doScrollLeft) && !scrollActive) {
            scrollActive = true
            postDelayed(scrollHandler, scrollDelay)
        }
    }
}