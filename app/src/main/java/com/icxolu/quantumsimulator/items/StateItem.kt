package com.icxolu.quantumsimulator.items

import android.os.Parcel
import com.icxolu.quantumsimulator.utils.Complex
import com.icxolu.quantumsimulator.utils.div
import com.icxolu.quantumsimulator.utils.parcelableCreator
import com.icxolu.quantumsimulator.utils.sqrt
import java.math.BigDecimal

class StateItem(): IItem {
    constructor(source: Parcel) : this() {
        state = States.valueOf(source.readString()!!)
    }

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(state.name)
    }

    var state: States = States.ZERO_STATE
        private set

    fun rollState() { state = state.next()}

    companion object {
        @JvmField val CREATOR = parcelableCreator(::StateItem)
        val ZERO_STATE = arrayOf(Complex.ONE, Complex.ZERO)
        val ONE_STATE = arrayOf(Complex.ZERO, Complex.ONE)
        val PLUS_STATE = arrayOf(Complex.ONE, Complex.ONE) / sqrt(BigDecimal("2"))
        val MINUS_STATE = arrayOf(Complex.ONE, -Complex.ONE) / sqrt(BigDecimal("2"))
        val PLUS_I_STATE = arrayOf(Complex.ONE, Complex.i) / sqrt(BigDecimal("2"))
        val MINUS_I_STATE = arrayOf(Complex.ONE, -Complex.i) / sqrt(BigDecimal("2"))
    }
}

enum class States {
    ZERO_STATE {
        override fun next() = ONE_STATE
        override val state: Array<Complex> get() = StateItem.ZERO_STATE
        override val text: String = "0"
    },
    ONE_STATE {
        override fun next(): States = PLUS_STATE
        override val state: Array<Complex> get() = StateItem.ONE_STATE
        override val text: String = "1"
    },
    PLUS_STATE {
        override fun next(): States = MINUS_STATE
        override val state: Array<Complex> get() = StateItem.PLUS_STATE
        override val text: String = "\u002b"
    },
    MINUS_STATE {
        override fun next(): States = PLUS_I_STATE
        override val state: Array<Complex> get() = StateItem.MINUS_STATE
        override val text: String = "\u2212"
    },
    PLUS_I_STATE {
        override fun next(): States = MINUS_I_STATE
        override val state: Array<Complex> get() = StateItem.PLUS_I_STATE
        override val text: String = "\u002bi"
    },
    MINUS_I_STATE {
        override fun next(): States = ZERO_STATE
        override val state: Array<Complex> get() = StateItem.MINUS_I_STATE
        override val text: String = "\u2212i"
    };

    abstract fun next(): States
    abstract val state: Array<Complex>
    abstract val text: String
}