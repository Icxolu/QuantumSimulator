package com.icxolu.quantumsimulator.items.gates

import android.os.Parcel
import android.text.SpannableString
import com.icxolu.quantumsimulator.R
import com.icxolu.quantumsimulator.items.MainGate
import com.icxolu.quantumsimulator.utils.Complex
import com.icxolu.quantumsimulator.utils.parcelableCreator
import com.icxolu.quantumsimulator.utils.supSpan

class SDaggerGate(): MainGate() {
    @Suppress("UNUSED_PARAMETER")
    constructor(source: Parcel) : this()

    override val spannableString: SpannableString = SpannableString("S\u2020").apply { supSpan(1, 2) }
    override val gateColorID: Int = R.attr.cCustomColor3Container
    override val gateBottomTintID: Int = R.attr.cCustomColor3Container
    override val matrix = MATRIX


    override fun writeToParcel(dest: Parcel, flags: Int) {}

    companion object {
        @JvmField val CREATOR = parcelableCreator(::SDaggerGate)
        val MATRIX = arrayOf(
                arrayOf(Complex.ONE, Complex.ZERO),
                arrayOf(Complex.ZERO, -Complex.i)
        )
    }
}