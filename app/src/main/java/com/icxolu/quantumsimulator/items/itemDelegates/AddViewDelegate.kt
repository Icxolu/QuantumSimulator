package com.icxolu.quantumsimulator.items.itemDelegates

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.icxolu.quantumsimulator.R
import com.icxolu.quantumsimulator.databinding.AddGateItemBinding
import com.icxolu.quantumsimulator.gateList.dialog.CreateGateDialog
import com.icxolu.quantumsimulator.items.AddView
import com.icxolu.quantumsimulator.items.IItem
import com.icxolu.quantumsimulator.recyclerDelegate.AbsDelegationAdapter
import com.icxolu.quantumsimulator.recyclerDelegate.AdapterDelegate

class AddViewDelegate: AdapterDelegate<IItem>() {
    override val layoutRes: Int = R.layout.add_gate_item

    override fun isForViewType(item: IItem): Boolean = item is AddView

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(adapter: AbsDelegationAdapter<IItem>, holder: RecyclerView.ViewHolder, items: List<IItem>, position: Int) {
        if (holder !is ViewHolder) throw RuntimeException()
        holder.bind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val img: ImageView = AddGateItemBinding.bind(itemView).imageView

        fun bind() {
            // img.setImageResource(R.drawable.baseline_add_24)
            itemView.setOnClickListener {
                CreateGateDialog.newInstance().show(activity.supportFragmentManager, null)
            }
        }
    }
}