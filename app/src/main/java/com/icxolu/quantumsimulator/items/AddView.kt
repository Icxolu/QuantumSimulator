package com.icxolu.quantumsimulator.items

import android.os.Parcel
import com.icxolu.quantumsimulator.utils.parcelableCreator

class AddView(): IItem {
    @Suppress("UNUSED_PARAMETER")
    constructor(source: Parcel) : this()

    override fun writeToParcel(dest: Parcel, flags: Int) { }

    companion object {
        @JvmField val CREATOR = parcelableCreator(::AddView)
    }
}