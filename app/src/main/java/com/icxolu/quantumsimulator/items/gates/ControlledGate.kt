package com.icxolu.quantumsimulator.items.gates

import android.os.Parcel
import com.icxolu.quantumsimulator.R
import com.icxolu.quantumsimulator.items.Gate
import com.icxolu.quantumsimulator.utils.Complex
import com.icxolu.quantumsimulator.utils.parcelableCreator
import com.icxolu.quantumsimulator.utils.using

class ControlledGate(): Gate() {
    constructor(source: Parcel): this() {
        isVisible = source.readInt() == 1
    }

    val drawable: Int = R.drawable.controlled_dot
    var isVisible: Boolean = false

    override val matrix = arrayOf(
            arrayOf(Complex.ONE, Complex.ZERO),
            arrayOf(Complex.ZERO, Complex.ONE)
    )
    override val isIdentity = true

    override fun writeToParcel(dest: Parcel, flags: Int) = using (dest) {
        writeInt(if (isVisible) 1 else 0)
    }

    companion object {
        @JvmField val CREATOR = parcelableCreator(::ControlledGate)
    }
}