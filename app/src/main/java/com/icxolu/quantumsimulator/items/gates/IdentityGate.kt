package com.icxolu.quantumsimulator.items.gates

import android.os.Parcel
import com.icxolu.quantumsimulator.items.Gate
import com.icxolu.quantumsimulator.utils.Complex
import com.icxolu.quantumsimulator.utils.parcelableCreator

class IdentityGate(): Gate() {
    @Suppress("UNUSED_PARAMETER")
    constructor(source: Parcel) : this()

    override val matrix = MATRIX
    override val isIdentity = true

    override fun writeToParcel(dest: Parcel, flags: Int) {}

    companion object {
        @JvmField val CREATOR = parcelableCreator(::IdentityGate)
        val MATRIX = arrayOf(
                arrayOf(Complex(1,0), Complex(0,0)),
                arrayOf(Complex(0,0), Complex(1,0))
        )
    }
}