package com.icxolu.quantumsimulator.items

import android.text.SpannableString
import androidx.annotation.AttrRes

abstract class MainGate: Gate() {
    abstract val spannableString: SpannableString
    @get:AttrRes abstract val gateColorID: Int
    @get:AttrRes abstract val gateBottomTintID: Int
}