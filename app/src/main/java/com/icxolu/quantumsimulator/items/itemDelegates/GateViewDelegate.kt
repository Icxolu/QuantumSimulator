package com.icxolu.quantumsimulator.items.itemDelegates

import android.content.ClipData
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView

import androidx.core.graphics.ColorUtils
import androidx.recyclerview.widget.RecyclerView

import com.icxolu.quantumsimulator.MainActivity
import com.icxolu.quantumsimulator.R
import com.icxolu.quantumsimulator.circuit.CircuitAdapter
import com.icxolu.quantumsimulator.databinding.GateViewItemBinding
import com.icxolu.quantumsimulator.gateList.GateListAdapter
import com.icxolu.quantumsimulator.items.Gate
import com.icxolu.quantumsimulator.items.IItem
import com.icxolu.quantumsimulator.items.MainGate
import com.icxolu.quantumsimulator.items.UserGate
import com.icxolu.quantumsimulator.items.gates.ControlledGate
import com.icxolu.quantumsimulator.items.gates.IdentityGate
import com.icxolu.quantumsimulator.recyclerDelegate.AbsDelegationAdapter
import com.icxolu.quantumsimulator.recyclerDelegate.AdapterDelegate
import com.icxolu.quantumsimulator.utils.MyOnDragListener
import com.icxolu.quantumsimulator.utils.UserGateView
import com.icxolu.quantumsimulator.utils.getColorFromAttr
import com.icxolu.quantumsimulator.utils.using

class GateViewDelegate: AdapterDelegate<IItem>() {
    override val layoutRes: Int = R.layout.gate_view_item

    override fun isForViewType(item: IItem): Boolean = item is Gate

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(adapter: AbsDelegationAdapter<IItem>, holder: RecyclerView.ViewHolder, items: List<IItem>, position: Int) {
        if (holder !is ViewHolder) throw RuntimeException()
        holder.bind(adapter, items[position] as Gate)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        private val bindings = GateViewItemBinding.bind(itemView)
        private val bg: ImageView = bindings.bg
        val img: ImageView = bindings.imageView
        val gate: UserGateView = bindings.gate

        fun bind(adapter: AbsDelegationAdapter<IItem>, item: Gate) {
            when (item) {
                is UserGate -> using(gate) {
                    visibility = View.VISIBLE
                    text = item.text
                    gateColor = item.gateColor
                    gateBottomTint = item.gateBottomTint
                }
                is MainGate -> using(gate) {
                    visibility = ImageView.VISIBLE
                    edit {
                        clear()
                        append(item.spannableString)
                    }
                    gateColor = activity.getColorFromAttr(item.gateColorID)
                    gateBottomTint = ColorUtils.setAlphaComponent(activity.getColorFromAttr(item.gateBottomTintID), 51)
                }
                is IdentityGate -> {
                    gate.visibility = View.GONE
                    bindings.imageView.visibility = View.GONE
                }
                is ControlledGate -> using(img) {
                    visibility = View.VISIBLE
                    item.isVisible = true
                    this@ViewHolder.gate.visibility = View.GONE
                    setImageResource(item.drawable)
                }
            }

            bg.visibility = ImageView.VISIBLE

            // All draggable gates in MainActivity except IdentityGate
            if ((adapter is GateListAdapter || (adapter is CircuitAdapter && adapter.areGatesDraggable)) && item !is IdentityGate) {

                // Start a drag
                itemView.setOnLongClickListener {

                    // Show delete button if dragged gate is UserGate or comes from circuit
                    if (item is UserGate || adapter is CircuitAdapter) {
                        (activity as MainActivity).binding.menuItemDeleteGate.visibility = Button.VISIBLE
                    }

                    // Hide circuit line for dragged gates
                    bg.visibility = ImageView.GONE

                    val data = ClipData.newPlainText("", "")
                    val builder = View.DragShadowBuilder(it)
                    it.startDragAndDrop(data, builder, DragObject(item, adapter is GateListAdapter, this), 0)

                    // Hide dragged gate from circuit
                    if (adapter is CircuitAdapter) {
                        gate.visibility = View.GONE
                        img.visibility = View.GONE
                        bg.visibility = View.VISIBLE
                        (item as? ControlledGate)?.isVisible = false
                    }
                    true
                }
            }

            // Set circuit as drop target and hide circuit lines for gateList
            when (adapter) {
                is CircuitAdapter -> itemView.setOnDragListener(MyOnDragListener(adapter, this))
                is GateListAdapter -> bg.visibility = ImageView.GONE
            }
        }
    }
}

data class DragObject(val item: IItem, val isGateListAdapter: Boolean, val holder: GateViewDelegate.ViewHolder)
