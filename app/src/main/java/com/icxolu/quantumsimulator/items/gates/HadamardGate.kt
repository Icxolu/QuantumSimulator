package com.icxolu.quantumsimulator.items.gates

import android.os.Parcel
import android.text.SpannableString
import com.icxolu.quantumsimulator.R
import com.icxolu.quantumsimulator.items.MainGate
import com.icxolu.quantumsimulator.utils.Complex
import com.icxolu.quantumsimulator.utils.div
import com.icxolu.quantumsimulator.utils.parcelableCreator
import com.icxolu.quantumsimulator.utils.sqrt
import java.math.BigDecimal

class HadamardGate(): MainGate() {
    @Suppress("UNUSED_PARAMETER")
    constructor(source: Parcel) : this()

    override val spannableString: SpannableString = SpannableString("H")
    override val gateColorID: Int = R.attr.cCustomColor2Container
    override val gateBottomTintID: Int = R.attr.cCustomColor2Container
    override val matrix = MATRIX


    override fun writeToParcel(dest: Parcel, flags: Int) {}

    companion object {
        @JvmField val CREATOR = parcelableCreator(::HadamardGate)
        val MATRIX = arrayOf(
                arrayOf(Complex.ONE, Complex.ONE),
                arrayOf(Complex.ONE, -Complex.ONE)
        ) / sqrt(BigDecimal(2))
    }
}