package com.icxolu.quantumsimulator.items.gates

import android.os.Parcel
import android.text.SpannableString
import com.icxolu.quantumsimulator.R
import com.icxolu.quantumsimulator.items.MainGate
import com.icxolu.quantumsimulator.utils.Complex
import com.icxolu.quantumsimulator.utils.parcelableCreator

class ZGate(): MainGate() {
    @Suppress("UNUSED_PARAMETER")
    constructor(source: Parcel) : this()

    override val spannableString: SpannableString = SpannableString("Z")
    override val gateColorID: Int = R.attr.cCustomColor1Container
    override val gateBottomTintID: Int = R.attr.cCustomColor1Container

    override val matrix = MATRIX


    override fun writeToParcel(dest: Parcel, flags: Int) {}

    companion object {
        @JvmField val CREATOR = parcelableCreator(::ZGate)
        val MATRIX = arrayOf(
                arrayOf(Complex.ONE, Complex.ZERO),
                arrayOf(Complex.ZERO, -Complex.ONE)
        )
    }
}