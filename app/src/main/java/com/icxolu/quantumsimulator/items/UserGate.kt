package com.icxolu.quantumsimulator.items

import android.os.Parcel
import androidx.annotation.ColorInt
import com.icxolu.quantumsimulator.utils.Complex
import com.icxolu.quantumsimulator.utils.parcelableCreator
import com.icxolu.quantumsimulator.utils.using

class UserGate(
    override val matrix: Array<Array<Complex>>,
    val text: String,
    @param:ColorInt val gateColor: Int,
    @param:ColorInt val gateBottomTint: Int
): Gate() {
    constructor(source: Parcel): this(
        Array(source.readInt()) { source.createTypedArray(Complex.CREATOR)!! },
            source.readString()!!,
            source.readInt(),
            source.readInt()
    )

    override fun writeToParcel(dest: Parcel, flags: Int) = using(dest) {
        writeInt(matrix.size)
        matrix.forEach {
            writeTypedArray(it, 0)
        }

        writeString(text)
        writeInt(gateColor)
        writeInt(gateBottomTint)
    }

    companion object {
        @JvmField val CREATOR = parcelableCreator(::UserGate)
    }
}