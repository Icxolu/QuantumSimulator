package com.icxolu.quantumsimulator.items.gates

import android.os.Parcel
import android.text.SpannableString
import com.icxolu.quantumsimulator.R
import com.icxolu.quantumsimulator.items.MainGate
import com.icxolu.quantumsimulator.utils.Complex
import com.icxolu.quantumsimulator.utils.parcelableCreator

class NotGate(): MainGate() {
    @Suppress("UNUSED_PARAMETER")
    constructor(source: Parcel) : this()

    override val spannableString: SpannableString = SpannableString("X")
    override val gateColorID: Int = R.attr.cCustomColor1Container
    override val gateBottomTintID: Int = R.attr.cCustomColor1Container

    override val matrix = MATRIX

    override fun writeToParcel(dest: Parcel, flags: Int) {}

    companion object {
        @JvmField val CREATOR = parcelableCreator(::NotGate)
        val MATRIX = arrayOf(
                arrayOf(Complex.ZERO, Complex.ONE),
                arrayOf(Complex.ONE, Complex.ZERO)
        )
    }
}