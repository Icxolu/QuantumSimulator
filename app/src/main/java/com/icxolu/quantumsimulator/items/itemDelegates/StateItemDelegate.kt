package com.icxolu.quantumsimulator.items.itemDelegates

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.icxolu.quantumsimulator.R
import com.icxolu.quantumsimulator.circuit.CircuitAdapter
import com.icxolu.quantumsimulator.databinding.StateViewItemBinding
import com.icxolu.quantumsimulator.items.IItem
import com.icxolu.quantumsimulator.items.StateItem
import com.icxolu.quantumsimulator.recyclerDelegate.AbsDelegationAdapter
import com.icxolu.quantumsimulator.recyclerDelegate.AdapterDelegate
import com.icxolu.quantumsimulator.utils.StateView
import com.icxolu.quantumsimulator.utils.subSpan

class StateItemDelegate: AdapterDelegate<IItem>() {
    override val layoutRes: Int = R.layout.state_view_item

    override fun isForViewType(item: IItem): Boolean = item is StateItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(adapter: AbsDelegationAdapter<IItem>, holder: RecyclerView.ViewHolder, items: List<IItem>, position: Int) {
        if (holder !is ViewHolder) throw RuntimeException()
        holder.bind(items[position] as StateItem)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val stateView: StateView = StateViewItemBinding.bind(itemView).state
        fun bind(item: StateItem) {
            if ((adapter as CircuitAdapter).areStatesClickable) {
                stateView.setOnClickListener {
                    item.rollState()
                    updateView(item)
                }
            }
            updateView(item)
        }

        private fun updateView(item: StateItem) {
            stateView.text = item.state.text
            stateView.edit {
                try {
                    subSpan(1, 2)
                } catch (e: IndexOutOfBoundsException) {}
            }
        }
    }
}