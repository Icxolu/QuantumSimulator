package com.icxolu.quantumsimulator.items

import com.icxolu.quantumsimulator.utils.Complex
import com.icxolu.quantumsimulator.utils.KParcelable

interface IItem: KParcelable

abstract class Gate: IItem {
    abstract val matrix: Array<Array<Complex>>
    open val isIdentity = false
}
