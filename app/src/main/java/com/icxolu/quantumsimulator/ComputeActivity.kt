package com.icxolu.quantumsimulator

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.ProgressBar

import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import java.util.*

import kotlin.collections.ArrayList
import kotlin.math.pow
import kotlinx.coroutines.*

import com.icxolu.quantumsimulator.barChart.BarChartAdapter
import com.icxolu.quantumsimulator.circuit.CircuitAdapter
import com.icxolu.quantumsimulator.databinding.ActivityComputeBinding
import com.icxolu.quantumsimulator.items.Gate
import com.icxolu.quantumsimulator.items.StateItem
import com.icxolu.quantumsimulator.items.gates.ControlledGate
import com.icxolu.quantumsimulator.utils.*

class ComputeActivity : AppCompatActivity() {
    companion object {
        private const val REQUEST_WRITE_EXTERNAL_STORAGE = 1
        private const val precision = 1E-4
        private const val BINARY_NOTATION_TV_TEXT = "binary_notation_tv_text"
        private const val BINARY_NOTATION_TV_TEXTSIZE = "binary_notation_tv_textsize"
        private const val BINARY_NOTATION_TV_TEXTCOLORS = "binary_notation_tv_textcolors"
        private const val LINE_CHART_LABEL = "line_chart_label"
    }
    private val circuitAdapter: CircuitAdapter by lazy {
        intent.getParcelableExtraSDK<CircuitAdapter>(MainActivity.CIRCUIT_ADAPTER)!!.apply {
            areGatesDraggable = false
            areStatesClickable = false
        }
    }
    private val manyQubits: Boolean by lazy { circuitAdapter.lines > 5 }
    private val barChartAdapter: BarChartAdapter by lazy { BarChartAdapter() }
    private val stateOutputAdapter: StateOutputAdapter by lazy { StateOutputAdapter(manyQubits = manyQubits, circuitLines = circuitAdapter.lines) }

    internal lateinit var binding: ActivityComputeBinding
    private lateinit var state: Array<Complex>
    private lateinit var binaryToState: HashMap<Int, Array<Complex>>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityComputeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /*if (manyQubits) {
            binding.infoCV.visibility = View.VISIBLE
            binding.binaryNotationCV.visibility = View.VISIBLE
            if (savedInstanceState != null) {
                binding.binaryNotationTV.text = savedInstanceState.getCharSequence(BINARY_NOTATION_TV_TEXT)
                binding.binaryNotationTV.textSize = savedInstanceState.getFloat(BINARY_NOTATION_TV_TEXTSIZE)
                binding.binaryNotationTV.setTextColor(savedInstanceState.getParcelable(BINARY_NOTATION_TV_TEXTCOLORS)!!)
            }
        } else {
            binding.infoCV.visibility = View.GONE
            binding.binaryNotationCV.visibility = View.GONE
        }*/

        setSupportActionBar(binding.toolbar)

        state = Array(2f.pow(circuitAdapter.lines).toInt()) { Complex.ONE }
        binaryToState = hashMapOf(
                0 to Array(2) {if (it == 0) Complex.ONE else Complex.ZERO},
                1 to Array(2) {if (it == 0) Complex.ZERO else Complex.ONE})

        lifecycleScope.launch (Dispatchers.Main) {
            compute().join()

            var stateStr = stateToString(state)

            stateOutputAdapter.set(stateStr)
            barChartAdapter.set(state.map { it.absSquare().toFloat() }, state.indices.map { "|" + deciToBinStr(it, circuitAdapter.lines) + "⟩" })
            /*binding.plotChart.data = state.map { it.absSquare().toFloat() }
            binding.plotChart.label = savedInstanceState?.getInt(LINE_CHART_LABEL) ?: -1*/
        }

        // binding.plotChart.manyQubits = manyQubits
        initStateOutputRV()
        initBarChart()
        initCircuitView()
    }

    private fun initStateOutputRV() {
        stateOutputAdapter.activity = this

        binding.stateOutputRV.apply {
            adapter = stateOutputAdapter
            layoutManager = LinearLayoutManager(this@ComputeActivity, RecyclerView.HORIZONTAL, false)
        }
    }

    private fun initBarChart() {
        barChartAdapter.activity = this

        binding.barChart.apply {
            adapter = barChartAdapter
            layoutManager = LinearLayoutManager(this@ComputeActivity, RecyclerView.VERTICAL, false)
        }
    }

    private fun initCircuitView() {
        circuitAdapter.activity = this

        binding.circuitRV.apply {
            adapter = circuitAdapter
            layoutManager = GridLayoutManager(this@ComputeActivity, circuitAdapter.lines, GridLayoutManager.HORIZONTAL, false)
            addItemDecoration(ItemDecorationControlled())
        }

        binding.shareCircuitBtn.setOnClickListener {
            shareCircuit()
        }
    }

    private fun stateToString(state: Array<Complex>): ArrayList<String> {

        val stateString = ArrayList<String>()

        for (i in state.indices) {
            if (state[i].abs() > precision) {
                when (manyQubits) {
                    false -> stateString.add(state[i].toStringMath() + "|" + deciToBinStr(i, circuitAdapter.lines) + "⟩")
                    true -> stateString.add(state[i].toStringMath() + "|" + i + "⟩")
                }
            }
        }
        stateString[0] = stateString[0].removePrefix(" +")
        
        return stateString
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(MainActivity.CIRCUIT_ADAPTER, circuitAdapter)
        /*outState.putCharSequence(BINARY_NOTATION_TV_TEXT, binding.binaryNotationTV.text)
        outState.putFloat(BINARY_NOTATION_TV_TEXTSIZE, convertPxToSp(binding.binaryNotationTV.textSize))
        outState.putParcelable(BINARY_NOTATION_TV_TEXTCOLORS, binding.binaryNotationTV.textColors)*/
        //outState.putInt(LINE_CHART_LABEL, binding.plotChart.label)
        super.onSaveInstanceState(outState)
    }

    private fun compute() = lifecycleScope.launch {

        // initialize state
        val myList = circuitAdapter.getColumnList(0).checkItemsAre<StateItem>()
        for (i in state.indices) {
            for (j in 0 until circuitAdapter.lines) {
                val index = (i/2f.pow(circuitAdapter.lines-j-1)).toInt().rem(2)
                state[i] *= myList[j].state.state[index]
            }
        }

        // iterate over all columns
        for (column in (application as Globals).rowsWithGates) {
            if (!isActive) break

            val subList = circuitAdapter.getColumnList(column).checkItemsAre<Gate>()
            val cGatesIndices = subList.filterIsInstance<ControlledGate>().map { subList.indexOf(it) }
            val areCGatesEmpty = cGatesIndices.isEmpty()
            var boolean = true

            // iterate over gates
            for (j in 0 until circuitAdapter.lines) {

                if (!subList[j].isIdentity) {
                    val tempList = ArrayList<Pair<Int, Complex>>()

                    // iterate over state
                    for (i in state.indices) {
                        if (state[i].abs() > precision) {
                            val stateBinary = deciToBin(i, circuitAdapter.lines)

                            if (!areCGatesEmpty) boolean = cGatesIndices.none { stateBinary[it] == 0 }

                            if (boolean) {
                                val tempState = matrixProductVector(binaryToState[stateBinary[j]]!!, subList[j].matrix)
                                val tempMultiplier = state[i]

                                state[i] *= tempState[stateBinary[j]]
                                stateBinary[j] = binaryAdd(stateBinary[j])
                                tempList.add(Pair(binToDeci(stateBinary), tempState[stateBinary[j]] * tempMultiplier))
                            }
                        }
                    }
                    tempList.forEach { state[it.first] += it.second }
                }
            }
        }
    }

    private fun matrixProductVector(a: Array<Complex>, b: Array<Array<Complex>>): Array<Complex> {
        val c = Array(a.size) { Complex.ZERO }

        for (i in a.indices) {
            for (j in a.indices) {
                c[i] += a[j] * b[i][j]
            }
        }

        return c
    }


    private fun binaryAdd(n: Int): Int {
        return when (n) {
            0 -> 1
            1 -> 0
            else -> throw IllegalStateException("Binary can only consist of '0' and '1'")
        }
    }

    /**
     * inflate options menu
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.compute_menu, menu)
        return true
    }

    /**
     * check the result of runtime permission request
     * if not granted show dialog the explain the user why the permission is needed
     * let the user accept and ask again or cancel the share operation
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_WRITE_EXTERNAL_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    shareCircuit()
                } else {
                    AlertDialog.Builder(this)
                            .setTitle(R.string.missing_permisssion_title)
                            .setMessage(R.string.missing_write_to_external_storage_permission)
                            .setPositiveButton(R.string.ok) { _, _ ->
                                requestPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, REQUEST_WRITE_EXTERNAL_STORAGE)
                            }
                            .setNegativeButton(R.string.cancel) { _, _ ->  }
                            .create()
                            .show()
                }
            }
        }
    }

    /**
     * share an image of the circuit
     */
    private fun shareCircuit() {
        val job = lifecycleScope.launch {
            val shareIntent = Intent(Intent.ACTION_SEND)
            val bitmap = generateBitmapFromCircuit().await()
            val uri = withContext(Dispatchers.Default) { saveImage(bitmap)}
            bitmap.recycle()
            shareIntent.apply {
                type = "image/png"
                putExtra(Intent.EXTRA_STREAM, uri)
            }
            doShare(shareIntent)
        }
        val dialog = AlertDialog.Builder(this)
                .setTitle(R.string.generateScreenshot)
                .setView(ProgressBar(this))
                .setNegativeButton(R.string.cancel) { _, _ ->
                    job.cancel()
                }
                .setOnCancelListener {
                    job.cancel()
                }
                .create()
        dialog.apply {
            setCanceledOnTouchOutside(false)
            show()
        }
        job.invokeOnCompletion {
            dialog.dismiss()
        }
    }

    /**
     * generate a bitmap from the circuit recyclerview
     */
    private fun generateBitmapFromCircuit(): Deferred<Bitmap> = lifecycleScope.async {
        val adapter = binding.circuitRV.adapter as CircuitAdapter
        // Measurespec
        val measureSpec = View.MeasureSpec.makeMeasureSpec(getDimensionResource(R.dimen.gate_size), View.MeasureSpec.EXACTLY)

        // Get width and height of items (should be equal for all items)
        val h = adapter.createViewHolder(binding.circuitRV, adapter.getItemViewType(0))
        adapter.onBindViewHolder(h, 0)
        h.itemView.measure(measureSpec, measureSpec)
        val itemWidth = h.itemView.measuredWidth
        val itemHeight = h.itemView.measuredHeight

        // Create bitmap with dimensions of recyclerview
        val bitmap = Bitmap.createBitmap(adapter.columns * itemWidth, adapter.lines * itemHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)

        // make background white
        canvas.drawColor(Color.WHITE)

        // iterate over items in adapter and draw each item on the canvas at the right position
        for (column in 0 until adapter.columns) {
            val gates = adapter.getColumnList(column)
            for (line in 0 until adapter.lines) {
                if (!isActive) break

                val pos = column * adapter.lines + line
                val holder = adapter.createViewHolder(binding.circuitRV, adapter.getItemViewType(pos))
                adapter.onBindViewHolder(holder, pos)

                using(holder.itemView) {
                    measure(measureSpec, measureSpec)
                    layout(0, 0, itemWidth, itemHeight)

                    canvas.save()
                    canvas.translate(column * itemWidth * 1f, line * itemHeight * 1f)
                    if (gates.any { it is ControlledGate } && adapter.lines > 1) {
                        drawControlLine(canvas, this, adapter, line)
                    }
                    draw(canvas)
                    canvas.restore()
                }
            }
        }
        bitmap
    }

    /**
     * start common share menu
     */
    private fun doShare(shareIntent: Intent) {
        startActivity(Intent.createChooser(shareIntent, "Share"))
    }
}
