# Quantum Circuit Simulator
An Android app to simulate the outcome of quantum circuits.
- Initialize a quantum circuit with up to 5 qubits
- Apply pre-defined quantum logic gates or define your own gates
- Simulate the circuit and visualize the statevector probabilities in a bar chart

## Screenshots
<p float="left">
  <img src="screenshots/screenshot_1.jpg" width="200" />
  <img src="screenshots/screenshot_2.jpg" width="200" /> 
  <img src="screenshots/screenshot_3.jpg" width="200" />
  <img src="screenshots/screenshot_4.jpg" width="200" />
</p>

## Supported quantum gates
- Clifford gates
  - Pauli gates: $X$, $Y$ and $Z$
  - Single qubit Clifford gates: $H$, $S$ and $S^{\dag}$
  - Two qubit Clifford gates: $CX$ ($CNOT$), $CY$ and $CZ$ ($CPhase$)
- Non-Clifford gates
  - $C3$ gates: $T$ and $T^{\dag}$
  - Standard rotation gates: $R_x(\theta)$, $R_y(\theta)$ and $R_z(\theta)$